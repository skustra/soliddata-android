package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VarKol(
    @JsonProperty("BGkolory")
    val BGkolory: BGkolory,
    @JsonProperty("PRV_szrafury")
    val PRV_szrafury: String,
    @JsonProperty("frak")
    val frak: String,
    @JsonProperty("kolory")
    val kolory: Kolory,
    @JsonProperty("szrafuryKat")
    val szrafuryKat: String,
    @JsonProperty("szrafurySkala")
    val szrafurySkala: String
) : Parcelable {
}