package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SondaRodzaj(
    @JsonProperty("LuderzenDlaID041")
    val LuderzenDlaID041: String,
    @JsonProperty("LuderzenDlaID067")
    val LuderzenDlaID067: String,
    @JsonProperty("maxLUderzen")
    val maxLUderzen: String,
    @JsonProperty("nazwa")
    val nazwa: String,
    @JsonProperty("rodzaj")
    val rodzaj: String,
    @JsonProperty("wbudowana")
    val wbudowana: String,
    @JsonProperty("wped")
    val wped: String
) : Parcelable {
}