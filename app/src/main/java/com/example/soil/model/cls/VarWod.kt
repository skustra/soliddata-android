package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VarWod(
    @JsonProperty("PWoda1")
    var PWoda1: Double
    ?,
    @JsonProperty("PWoda2")
    var PWoda2: Double
    ?
) : Parcelable {
}