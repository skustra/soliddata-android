package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ClsProf(
    @JsonProperty("Data")
    var Data: String?,
    @JsonProperty("Metryka")
    val Metryka: String,
    @JsonProperty("Nr")
    var Nr: String,
    @JsonProperty("Prof_ID")
    private val profileId: String?,
    @JsonProperty("RzednaOtworu")
    val RzednaOtworu: RzednaOtworu,
    @JsonProperty("Samples")
    var Samples: Samples?,
    @JsonProperty("SondaData")
    var SondaData: String?,
    @JsonProperty("SondaGleb")
    val SondaGleb: SondaGleb,
    @JsonProperty("SondaRodzaj")
    val SondaRodzaj: SondaRodzaj?,
/*    @JsonProperty("Sondowanie")
    val Sondowanie: Any,*/
    @JsonProperty("X")
    var X: Double?,
    @JsonProperty("Y")
    var Y: Double?,
    @JsonProperty("nazwa")
    val nazwa: String?,
    @JsonProperty("saczenia")
    var saczenia: Saczenia?,
    @JsonProperty("skalaSondaindex")
    val skalaSondaindex: String?,
    @JsonProperty("skalaindex")
    val skalaindex: String?,
    @JsonProperty("sondaRysuj")
    val sondaRysuj: String?,
    @JsonProperty("warstwy")
    val warstwy: Warstwy,
    @JsonProperty("wody")
    var wody: Wody?
) : Parcelable