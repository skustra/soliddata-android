package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Wody(
    @JsonProperty("var_wod")
    var var_wod: ArrayList<VarWod>?
) : Parcelable {
}