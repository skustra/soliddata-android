package com.example.soil.model.states

data class Ground(
    val stan: String,
    val stanik: String,
    val stanik_Pelna: String,
    val str: String,
    var cohesive: Boolean = false
)