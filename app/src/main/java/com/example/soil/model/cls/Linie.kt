package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Linie(
    @JsonProperty("var_Linia")
    val var_Linia: List<VarLinia>
) : Parcelable {
}