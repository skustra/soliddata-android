package com.example.soil.model.ground

data class SoilItemsResponse(
    val g1: List<GroundResponseItem>,
    val g2: List<GroundResponseItem>,
    val g3: List<GroundResponseItem>
)