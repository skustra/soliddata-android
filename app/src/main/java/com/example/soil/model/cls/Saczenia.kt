package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Saczenia(
    @JsonProperty("var_sacz")
    val var_sacz: ArrayList<VarSacz>?
) : Parcelable