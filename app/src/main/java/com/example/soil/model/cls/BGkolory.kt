package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BGkolory(
    @JsonProperty("A")
    val A: String,
    @JsonProperty("B")
    val B: String,
    @JsonProperty("G")
    val G: String,
    @JsonProperty("R")
    val R: String
) : Parcelable {
}