package com.example.soil.model.ground

data class GroundResponseItem(
    val frak: String,
    val nazwaGlowna: String,
    val number: Int
)