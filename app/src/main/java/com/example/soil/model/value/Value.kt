package com.example.soil.model.value

data class Value constructor(
    val id: String,
    val displayValue: String,
    var selected: Boolean = false
)