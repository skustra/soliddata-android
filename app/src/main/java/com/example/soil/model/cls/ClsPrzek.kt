package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ClsPrzek(
    @JsonProperty("Linie")
    val Linie: Linie?,
    @JsonProperty("PrzekDEF")
    val PrzekDEF: String,
    @JsonProperty("PrzekNrProfilu")
    val PrzekNrProfilu: PrzekNrProfilu,
    @JsonProperty("PrzekRys")
    val PrzekRys: String,
    @JsonProperty("PrzekSkalaPion")
    val PrzekSkalaPion: String,
    @JsonProperty("PrzekSkalaPoz")
    val PrzekSkalaPoz: String
) : Parcelable {
}