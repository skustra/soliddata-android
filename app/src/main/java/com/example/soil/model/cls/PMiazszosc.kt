package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PMiazszosc(
    @JsonProperty("mm")
    var mm: Double = 0.0
) : Parcelable {
}