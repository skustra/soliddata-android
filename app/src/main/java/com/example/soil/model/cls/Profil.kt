package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Profil(
    @JsonProperty("cls_Prof")
    var cls_Prof: ArrayList<ClsProf> = arrayListOf()
) : Parcelable
