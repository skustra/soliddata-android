package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VarSacz(
    @JsonProperty("PSacz1")
    var PSacz1: Double?,
    @JsonProperty("PSacz2")
    var PSacz2: Double?
) : Parcelable