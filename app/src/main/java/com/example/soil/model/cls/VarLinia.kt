package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VarLinia(
    @JsonProperty("lin1")
    val lin1: String,
    @JsonProperty("lin2")
    val lin2: String,
    @JsonProperty("prof1")
    val prof1: String,
    @JsonProperty("prof2")
    val prof2: String,
    @JsonProperty("stala")
    val stala: String,
    @JsonProperty("wez1")
    val wez1: String,
    @JsonProperty("wez2")
    val wez2: String,
    @JsonProperty("x1")
    val x1: String,
    @JsonProperty("x2")
    val x2: String,
    @JsonProperty("y1")
    val y1: String,
    @JsonProperty("y2")
    val y2: String
) : Parcelable {
}