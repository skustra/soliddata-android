package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Plkkolory(
    @JsonProperty("kol")
    val kol: Kol?,
    @JsonProperty("wersja") val
    wersja: String
) : Parcelable {
}