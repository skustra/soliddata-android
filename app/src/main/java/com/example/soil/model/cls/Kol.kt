package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Kol(
    @JsonProperty("var_kol")
    val var_kol: List<VarKol>
) : Parcelable