package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SoilModel(
    @JsonProperty("cls_Project")
    @SerializedName("cls_Project")
    val cls_Project: ClsProject
) : Parcelable {
}