package com.example.soil.model.humidities

data class HumiditiesResponseItem(
    val pelnaNazwa: String,
    val str: String,
    val wilg: String
)