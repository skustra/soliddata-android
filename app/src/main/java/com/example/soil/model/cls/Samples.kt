package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Samples(
    @JsonProperty("cls_Sample")
    val cls_Sample: ArrayList<ClsSample>?
) : Parcelable {
}