package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ClsWarstwaGeo(
    @JsonProperty("NrWarGeo")
    val NrWarGeo: String,
    @JsonProperty("opisWar")
    val opisWar: String,
    @JsonProperty("stan")
    val stan: String,
    @JsonProperty("wilg")
    val wilg: String
) : Parcelable {
}