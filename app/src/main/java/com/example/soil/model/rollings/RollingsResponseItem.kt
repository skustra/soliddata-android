package com.example.soil.model.rollings

data class RollingsResponseItem(
    val lWal: String,
    val str: String,
    val value: Int
)