package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ClsProject(
    @JsonProperty("Euro")
    var Euro: Boolean,
    @JsonProperty("GPS")
    val GPS: Boolean?,
    @JsonProperty("NazwaProjektu")
    var NazwaProjektu: String?,
    @JsonProperty("Profil")
    var Profil: Profil? = Profil(),
    @JsonProperty("Proj_ID")
    val Proj_ID: String?,
    @JsonProperty("wersja")
    val wersja: String?,
) : Parcelable {
}