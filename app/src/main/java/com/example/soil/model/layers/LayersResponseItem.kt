package com.example.soil.model.layers

data class LayersResponseItem(
    val frak: String,
    val nazwaDodatku: String,
    val nazwaGlowna: String,
    val nazwaPrzewarstwienia: String,
    val skrot: String,
    val str: String
)