package com.example.soil.model.cls

import com.google.gson.annotations.SerializedName

data class Model(
    @SerializedName("SoilModel")
    val SoilModel: SoilModel
)
