package com.example.soil.model.cls

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VarGrunt(
    @JsonProperty("G1")
    var G1: String,
    @JsonProperty("G2")
    var G2: String,
    @JsonProperty("G3")
    var G3: String,
    @JsonProperty("NOR_e")
    var NOR_e: String,
    @JsonProperty("NrWarGeo")
    var NrWarGeo: String,
    @JsonProperty("PColour")
    var PColour: String,
    @JsonProperty("PMiazszosc")
    val PMiazszosc: PMiazszosc,
    @JsonProperty("PNOR")
    val PNOR: PNOR,
    @JsonProperty("POpisWarstwy")
    var POpisWarstwy: String?,
    @JsonProperty("PPNG2")
    val PPNG2: String,
    @JsonProperty("PWilgot")
    val PWilgot: PWilgot,
    @JsonProperty("wilg_e")
    var wilg_e: String,
    @JsonProperty("xml_Stan")
    var xml_Stan: String
) : Parcelable {
}