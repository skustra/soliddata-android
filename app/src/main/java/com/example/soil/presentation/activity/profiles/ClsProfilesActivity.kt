package com.example.soil.presentation.activity.profiles

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.soil.R
import com.example.soil.model.cls.Profil
import com.example.soil.model.cls.SoilModel
import com.example.soil.presentation.activity.BaseActivity
import com.example.soil.presentation.activity.ground.EditGroundLayersActivity
import com.example.soil.presentation.activity.import.ImportFileActivity
import com.example.soil.presentation.fragment.profiles.map.MapFragment
import com.example.soil.presentation.fragment.profiles.profiles.ClsProfilesFragment
import com.example.soil.usecase.ExportSoilModelUseCase
import com.example.soil.usecase.MockSoilModel
import com.example.soil.utils.Constants.PROFILE_NUMBER
import com.example.soil.utils.Constants.SOIL_MODEL
import com.example.soil.utils.DateUtils
import com.example.soil.utils.RequestCodes.EDIT_FILE_CODE
import com.example.soil.utils.listen
import com.example.soil.utils.replaceFragment
import com.example.soil.utils.setSafeOnClickListener
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import kotlinx.android.synthetic.main.activity_navigation.*
import java.text.SimpleDateFormat
import java.util.*

class ClsProfilesActivity : BaseActivity() {

    private lateinit var soilModel: SoilModel

    private lateinit var preferences: SharedPreferences

    internal val viewModel: FileNavigationViewModel by lazy {
        provideViewModel(FileNavigationViewModel::class.java)
    }

    override fun onBackPressed() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        preferences = getPreferences(Context.MODE_PRIVATE)
        soilModel = intent.getParcelableExtra<SoilModel>(SOIL_MODEL) as SoilModel

        if (soilModel.cls_Project.Profil == null) {
            soilModel.cls_Project.Profil = Profil()
        }

        tabLayout.getTabAt(1)?.select()
        tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}
            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> viewModel.changeNavigation(FileNavigation.ClsProfilesScreen)
                    1 -> viewModel.changeNavigation(FileNavigation.ClsMapScreen)
                }
            }
        })

        exportFile.setSafeOnClickListener {
            viewModel.changeNavigation(FileNavigation.ExportFile)
        }

        addProfile.setSafeOnClickListener {
            val profiles = soilModel.cls_Project.Profil?.cls_Prof ?: return@setSafeOnClickListener

            profiles.add(
                MockSoilModel().getEmptyProfile(
                    number = (soilModel.cls_Project.Profil?.cls_Prof?.size ?: 0) + 1,
                    latitude = preferences.getFloat("lat", 0f).toDouble(),
                    longitude = preferences.getFloat("lng", 0f).toDouble()
                )
            )
            updateUi()

            viewModel.changeNavigation(
                FileNavigation.ClsProfileDetails(
                    soilModel,
                    profiles.last().Nr
                )
            )
        }

        viewModel.getNavigation().listen(this) { navigation ->
            when (navigation) {
                is FileNavigation.ExportFile -> showExportFileDialog()
                is FileNavigation.ClsMapScreen -> {
                    replaceFragment(MapFragment.getInstance(soilModel), R.id.fragmentContainer)
                }
                is FileNavigation.ClsProfilesScreen -> {
                    replaceFragment(
                        ClsProfilesFragment.getInstance(soilModel),
                        R.id.fragmentContainer
                    )
                }
                is FileNavigation.ClsProfileDetails -> {
                    startActivityForResult(
                        Intent(this, EditGroundLayersActivity::class.java).apply {
                            putExtra(SOIL_MODEL, navigation.soilModel)
                            putExtra(PROFILE_NUMBER, navigation.number)
                        }, EDIT_FILE_CODE
                    )
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == EDIT_FILE_CODE && resultCode == Activity.RESULT_OK) {
            soilModel = data?.getParcelableExtra<SoilModel>(SOIL_MODEL) as SoilModel
            updateUi()
        }
    }

    private fun updateUi() {
        val mapSelected = tabLayout.getTabAt(1)?.isSelected ?: false

        if (mapSelected) {
            viewModel.changeNavigation(FileNavigation.ClsMapScreen)
            tabLayout.getTabAt(1)?.select()
        } else {
            viewModel.changeNavigation(FileNavigation.ClsProfilesScreen)
            tabLayout.getTabAt(0)?.select()
        }
    }

    private fun showExportFileDialog() {
        val fileName =
            "GPS_${SimpleDateFormat(DateUtils.FRONT_END_DATE_FORMAT, Locale.getDefault()).format(
                Date()
            )}"
        AlertDialog.Builder(this)
            .create()
            .apply {
                setTitle(getString(R.string.export_file_title))
                setMessage(("${getString(R.string.export_file_message)} ${ExportSoilModelUseCase().getFilePath()}/$fileName.SoilData"))
                setButton(Dialog.BUTTON_POSITIVE, getString(R.string.ok)) { dialog, _ ->
                    dialog.dismiss()
                    exportFile(fileName)
                }
                setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                show()
            }
    }

    private fun exportFile(fileName: String) {
        ExportSoilModelUseCase().invoke(
            this,
            soilModel,
            fileName
        )

      //  startActivity(Intent(this, ImportFileActivity::class.java))
    }


    fun openProfileDetails(profileNumber: String) {
        viewModel.changeNavigation(
            FileNavigation.ClsProfileDetails(
                soilModel,
                profileNumber
            )
        )
    }

    fun removeProfile(index: Int) {
        val profiles = soilModel.cls_Project.Profil?.cls_Prof

        if (profiles != null) {
            profiles.removeAt(index)

            for (i in profiles.indices) {
                profiles[i].Nr = (i + 1).toString()
            }

            updateUi()
        }
    }
}