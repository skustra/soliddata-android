package com.example.soil.presentation.fragment.ground.layers.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.soil.R
import com.example.soil.model.cls.VarGrunt
import com.example.soil.presentation.activity.ground.EditGroundLayersActivity
import com.example.soil.utils.millimetreToMeter
import utils.round
import kotlin.reflect.KFunction1

class GroundLayersAdapter(
    val activity : EditGroundLayersActivity,
    val data: List<VarGrunt>,
    val pickGroundLayer: KFunction1<@ParameterName(name = "groundIndex") Int, Unit>,
    val removeGroundLater: KFunction1<@ParameterName(name = "groundIndex") Int, Unit>

) :
    RecyclerView.Adapter<GroundLayersAdapter.LayersViewHolder>() {

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LayersViewHolder {
        return LayersViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_ground_layer, parent, false)
        )
    }

    override fun onBindViewHolder(holder: LayersViewHolder, position: Int) {
        val item = data[position]
        val groundPosition = position + 1
        holder.clsPosition.text = groundPosition.toString()
        holder.thickness.text = "${item.PMiazszosc.mm.millimetreToMeter().round(2)} m"
        holder.ground.text = activity.viewModel.findG1Ground(item.G1)

        val oneBeforeLastThickness = if (position > 0) {
            data[position - 1].PMiazszosc.mm
        } else 0.0

        holder.thicknessSum.text =
            "${(item.PMiazszosc.mm + oneBeforeLastThickness).millimetreToMeter().round(2)} m ppt"


        holder.container.setOnClickListener {
            pickGroundLayer(position)
        }

        holder.container.setOnLongClickListener {
            removeGroundLater(position)
            true
        }
    }

    class LayersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ground: TextView = view.findViewById(R.id.ground)
        val thickness: TextView = view.findViewById(R.id.thickness)
        val thicknessSum: TextView = view.findViewById(R.id.thicknessSum)
        val clsPosition: TextView = view.findViewById(R.id.clsPosition)
        val container: LinearLayout = view.findViewById(R.id.container)
    }
}