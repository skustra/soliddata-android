package com.example.soil.presentation.activity.water


import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.soil.R
import com.example.soil.model.cls.SoilModel
import com.example.soil.presentation.activity.BaseActivity
import com.example.soil.presentation.fragment.water.details.WaterLayersDetailsFragment
import com.example.soil.presentation.fragment.water.edit.EditWaterLayersFragment
import com.example.soil.utils.Constants.PROFILE_NUMBER
import com.example.soil.utils.Constants.SOIL_MODEL
import com.example.soil.utils.addFragmentWithBackStack
import com.example.soil.utils.listen
import com.example.soil.utils.replaceFragment
import com.example.soil.utils.toJson
import kotlinx.android.synthetic.main.activity_edit_water.*
import timber.log.Timber

class EditWaterLayersActivity : BaseActivity() {

    val viewModel: EditWaterLayersViewModel by lazy {
        provideViewModel(EditWaterLayersViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_water)

        backArrow.setOnClickListener {
            onBackPressed()
        }

        val soilModel = intent.getParcelableExtra<SoilModel>(SOIL_MODEL) ?: return
        val profileNumber = intent.getStringExtra(PROFILE_NUMBER) ?: return
        viewModel.setData(soilModel, profileNumber)

        viewModel.getNavigation().listen(this) { field ->

            when (field) {
                is Field.EditWater,
                is Field.EditFiltration,
                is Field.EditSamples -> {
                    addFragmentWithBackStack(
                        EditWaterLayersFragment.getInstance(
                            field
                        ),
                        R.id.contentFragmentContainer
                    )
                }
                is Field.WaterLayersDetails -> {
                    replaceFragment(
                        WaterLayersDetailsFragment.getInstance(),
                        R.id.contentFragmentContainer
                    )
                }
            }
        }
    }

    override fun finish() {
        val soilModel = viewModel.getUpdatedSoilModel()
        if (soilModel != null) {
            val data = Intent()
                .apply {
                    putExtras(Bundle().apply {
                        putParcelable(SOIL_MODEL, soilModel)
                        Timber.d("soilModel %s", soilModel.cls_Project?.Profil?.cls_Prof?.toJson())
                    })
                }
            setResult(Activity.RESULT_OK, data)
        }
        super.finish()
    }

    fun remove(field: Field) {
        AlertDialog.Builder(this)
            .create()
            .apply {
                setTitle(getString(R.string.attention))
                setMessage(getString(R.string.remove_item_question))
                setButton(Dialog.BUTTON_POSITIVE, getString(R.string.yes)) { dialog, _ ->
                    viewModel.removeItem(field)
                    dialog.dismiss()

                }
                setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.no)) { dialog, _ ->
                    dialog.dismiss()
                }
                show()
            }
    }
}