package com.example.soil.presentation.fragment.profiles.profiles

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.example.soil.R
import com.example.soil.model.cls.SoilModel
import com.example.soil.presentation.activity.profiles.ClsProfilesActivity
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.presentation.fragment.profiles.profiles.asapter.ClsAdapter
import com.example.soil.utils.Constants.SOIL_MODEL
import kotlinx.android.synthetic.main.fragment_cls_profiles.*

class ClsProfilesFragment : BaseFragment() {

    private lateinit var clsProfilesActivity: ClsProfilesActivity

    companion object {

        fun getInstance(soilModel: SoilModel): ClsProfilesFragment {
            return ClsProfilesFragment()
                .apply {
                    arguments = Bundle().apply {
                        putParcelable(SOIL_MODEL, soilModel)
                    }
                }
        }
    }

    override fun setLayout() = R.layout.fragment_cls_profiles

    override fun onAttach(context: Context) {
        super.onAttach(context)
        clsProfilesActivity = context as ClsProfilesActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val clsProf = arguments?.getParcelable<SoilModel>(SOIL_MODEL) as SoilModel
        val profile = clsProf.cls_Project.Profil ?: return
        val adapter = ClsAdapter(profile.cls_Prof, ::openProfile, ::removeProfile)
        clsProfileRecyclerView.adapter = adapter
    }

    private fun openProfile(profileNumber: String) {
        clsProfilesActivity.openProfileDetails(profileNumber)
    }

    private fun removeProfile(index: Int) {
        AlertDialog.Builder(clsProfilesActivity)
            .create()
            .apply {
                setTitle(getString(R.string.attention))
                setMessage(getString(R.string.remove_item_question))
                setButton(Dialog.BUTTON_POSITIVE, getString(R.string.yes)) { dialog, _ ->
                    clsProfilesActivity.removeProfile(index)
                    dialog.dismiss()

                }
                setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.no)) { dialog, _ ->
                    dialog.dismiss()
                }
                show()
            }
    }
}