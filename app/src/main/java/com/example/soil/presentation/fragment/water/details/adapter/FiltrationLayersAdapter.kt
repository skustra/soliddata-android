package com.example.soil.presentation.fragment.water.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.soil.R
import com.example.soil.model.cls.VarSacz
import utils.shouldBeVisibleIf
import kotlin.reflect.KFunction1

class FiltrationLayersAdapter(
    val data: List<VarSacz>,
    val editValue: KFunction1<@ParameterName(name = "value") Int, Unit>,
    val removeValue: KFunction1<@ParameterName(name = "value") Int, Unit>
) :
    RecyclerView.Adapter<FiltrationLayersAdapter.LayersViewHolder>() {

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LayersViewHolder {
        return LayersViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_water, parent, false)
        )
    }

    override fun onBindViewHolder(holder: LayersViewHolder, position: Int) {
        val item = data[position]

        val firstValue = item.PSacz1 ?: 0.0
        val secondValue = item.PSacz2 ?: 0.0

        holder.firstLayer.text = "$firstValue"
        holder.secondLayer.text = "$secondValue"

        holder.valueContainer.setOnClickListener {
            editValue(position)
        }
        holder.position.text = (position + 1).toString()
        holder.valueContainer.setOnLongClickListener {
            removeValue(position)
            true
        }

        holder.header.shouldBeVisibleIf(position == 0)
    }

    class LayersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val firstLayer: TextView = view.findViewById(R.id.first)
        val secondLayer: TextView = view.findViewById(R.id.second)
        val valueContainer: LinearLayout = view.findViewById(R.id.valueContainer)
        val position: TextView = view.findViewById(R.id.position)
        val header: LinearLayout = view.findViewById(R.id.header)
    }
}