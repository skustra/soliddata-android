package com.example.soil.presentation.fragment.ground.header

import android.content.Context
import android.os.Bundle
import android.view.View
import com.example.soil.R
import com.example.soil.presentation.activity.ground.EditGroundLayersActivity
import com.example.soil.presentation.activity.ground.Field
import com.example.soil.presentation.activity.ground.Navigation
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.utils.DateUtils.Companion.formatDate
import com.example.soil.utils.listen
import com.example.soil.utils.millimetreToMeter
import kotlinx.android.synthetic.main.fragment_layer_header.*

class LayerHeaderFragment : BaseFragment() {

    private lateinit var groundLayersActivity: EditGroundLayersActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        groundLayersActivity = context as EditGroundLayersActivity
    }

    override fun setLayout() = R.layout.fragment_layer_header

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        groundLayersActivity.viewModel.getClsProfile().listen(viewLifecycleOwner) {
            date.text = formatDate(it.SondaData ?: "")
            ordinateOfMeasurement.text = "${it.RzednaOtworu.mm.millimetreToMeter()} m ppt"
            latitude.text = it.Y?.toString() ?: "0.0"
            longitude.text = it.X?.toString() ?: "0.0"
            profileNumber.text = it.Nr
        }

        latitudeContainer.setOnClickListener { edit(Field.Latitude) }
        longitudeContainer.setOnClickListener { edit(Field.Longitude) }
        dateContainer.setOnClickListener { edit(Field.Date) }
        ordinateOfMeasurementContainer.setOnClickListener { edit(Field.OrdinateOfMesurement) }
    }

    private fun edit(field: Field) {
        groundLayersActivity.viewModel.changeNavigation(Navigation.EditClsProfile(field))
    }
}