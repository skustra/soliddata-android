package com.example.soil.presentation.activity.import

import android.content.Intent
import android.os.Bundle
import com.example.soil.R
import com.example.soil.presentation.activity.BaseActivity
import com.example.soil.presentation.activity.profiles.ClsProfilesActivity
import com.example.soil.presentation.fragment.files.FilesFragment
import com.example.soil.presentation.fragment.import.ImportFileFragment
import com.example.soil.presentation.fragment.splash.SplashFragment
import com.example.soil.utils.Constants.SOIL_MODEL
import com.example.soil.utils.listen
import com.example.soil.utils.replaceActivity
import com.example.soil.utils.replaceFragment

class ImportFileActivity : BaseActivity() {

    internal val viewModel: ImportFileNavigationViewModel by lazy {
        provideViewModel(ImportFileNavigationViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_import_file)

        viewModel.getNavigation().listen(this) { navigation ->
            when (navigation) {
                is Navigation.SplashScreen -> replaceFragment(
                    SplashFragment(),
                    R.id.fragmentContainer
                )
                is Navigation.ImportFileScreen -> replaceFragment(
                    ImportFileFragment(),
                    R.id.fragmentContainer
                )
                is Navigation.FilesScreen -> replaceFragment(
                    FilesFragment(),
                    R.id.fragmentContainer
                )
                is Navigation.FileNavigationScreen -> {
                    replaceActivity(Intent(this, ClsProfilesActivity::class.java).apply {
                        putExtra(SOIL_MODEL, navigation.data)
                    })
                }
            }
        }
    }
}