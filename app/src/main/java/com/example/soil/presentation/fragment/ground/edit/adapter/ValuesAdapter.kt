package com.example.soil.presentation.fragment.ground.edit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.soil.R
import com.example.soil.model.value.Value
import timber.log.Timber
import kotlin.reflect.KFunction1

class ValuesAdapter(
    val data: List<Value>,
    val pickValue: KFunction1<@ParameterName(name = "value") Value, Unit>
) :
    RecyclerView.Adapter<ValuesAdapter.LayersViewHolder>() {

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LayersViewHolder {
        return LayersViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_value, parent, false)
        )
    }

    override fun onBindViewHolder(holder: LayersViewHolder, position: Int) {
        val item = data[position]

        holder.value.text = item.displayValue

        holder.radioButton.setOnClickListener {
            selectValue(item, position)
        }
        holder.valueContainer.setOnClickListener {
            selectValue(item, position)
        }

        holder.radioButton.isChecked = item.selected
    }

    private fun selectValue(item: Value, position: Int) {
        Timber.d("Selected: %s", item.id)
        pickValue(item)
        data.forEach { it.selected = false }
        data[position].selected = true
        notifyDataSetChanged()
    }

    class LayersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val radioButton: RadioButton = view.findViewById(R.id.radioButton)
        val value: TextView = view.findViewById(R.id.value)
        val valueContainer: LinearLayout = view.findViewById(R.id.valueContainer)
    }
}