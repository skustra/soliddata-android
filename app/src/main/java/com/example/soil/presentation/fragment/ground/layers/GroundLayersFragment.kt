package com.example.soil.presentation.fragment.ground.layers

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.example.soil.R
import com.example.soil.presentation.activity.ground.EditGroundLayersActivity
import com.example.soil.presentation.activity.ground.Navigation
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.presentation.fragment.ground.layers.adapter.GroundLayersAdapter
import com.example.soil.utils.listen
import com.example.soil.utils.setSafeOnClickListener
import kotlinx.android.synthetic.main.fragment_ground_layers.*

class GroundLayersFragment : BaseFragment() {

    private lateinit var groundLayersActivity: EditGroundLayersActivity

    override fun setLayout() = R.layout.fragment_ground_layers

    override fun onAttach(context: Context) {
        super.onAttach(context)
        groundLayersActivity = context as EditGroundLayersActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        groundLayersActivity.viewModel.getClsProfile().listen(viewLifecycleOwner) { clsProf ->
            val adapter = GroundLayersAdapter(
                groundLayersActivity,
                clsProf.warstwy.var_grunt,
                ::openGroundDetails,
                ::removeGroundLayer
            )
            groundLayersRecyclerView.adapter = adapter

            addWaterLayers.setSafeOnClickListener {
                groundLayersActivity.viewModel.changeNavigation(Navigation.WaterLayers(clsProf = clsProf))
            }
        }

        addLayer.setSafeOnClickListener {
            groundLayersActivity.viewModel.addGroundLayer()
        }
    }

    private fun openGroundDetails(groundIndex: Int) {
        groundLayersActivity.viewModel.changeNavigation(Navigation.GroundLayerDetails(groundIndex))
    }

    private fun removeGroundLayer(groundIndex: Int) {
        AlertDialog.Builder(groundLayersActivity)
            .create()
            .apply {
                setTitle(getString(R.string.attention))
                setMessage(getString(R.string.remove_item_question))
                setButton(Dialog.BUTTON_POSITIVE, getString(R.string.yes)) { dialog, _ ->
                    groundLayersActivity.viewModel.removeGroundLayer(groundIndex)
                    dialog.dismiss()

                }
                setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.no)) { dialog, _ ->
                    dialog.dismiss()
                }
                show()
            }
    }
}