package com.example.soil.presentation.activity

import android.content.Context
import android.view.WindowManager
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.multidex.MultiDex

open class BaseActivity : AppCompatActivity() {

    fun <T : ViewModel> provideViewModel(@NonNull modelClass: Class<T>): T {
        return ViewModelProviders
            .of(this)
            .get(modelClass)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
    }

    override fun attachBaseContext(context: Context?) {
        super.attachBaseContext(context)
        MultiDex.install(this)
    }
}