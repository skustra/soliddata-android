package com.example.soil.presentation.fragment.profiles.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import com.example.soil.R
import com.example.soil.model.cls.SoilModel
import com.example.soil.presentation.activity.profiles.ClsProfilesActivity
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.presentation.fragment.profiles.map.markers.markers.Group
import com.example.soil.presentation.fragment.profiles.map.markers.markers.MarkerRender
import com.example.soil.utils.Constants.SOIL_MODEL
import com.example.soil.utils.toJson
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterManager
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.fragment_map.*
import timber.log.Timber


class MapFragment : BaseFragment(), OnMapReadyCallback {

    private lateinit var navigationActivity: ClsProfilesActivity

    private lateinit var soilModel: SoilModel

    private lateinit var clusterManager: ClusterManager<Group>

    private lateinit var googleMap: GoogleMap

    private var zoomLevel = 17f

    private lateinit var preferences: SharedPreferences

    companion object {
        fun getInstance(soilModel: SoilModel): MapFragment {
            return MapFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(SOIL_MODEL, soilModel)
                }
            }
        }
    }

    override fun setLayout() = R.layout.fragment_map

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        soilModel = arguments?.getParcelable<SoilModel>(SOIL_MODEL) as SoilModel
        preferences = navigationActivity.getPreferences(Context.MODE_PRIVATE)
        checkPermissions()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigationActivity = context as ClsProfilesActivity
    }

    override fun onMapReady(map: GoogleMap) {
        this.googleMap = map


        setMarkers()
        changeMapTyp.setOnClickListener {
            if (map.mapType != GoogleMap.MAP_TYPE_SATELLITE) {
                map.mapType = GoogleMap.MAP_TYPE_SATELLITE
            } else {
                map.mapType = GoogleMap.MAP_TYPE_NORMAL
            }
        }

        googleMap.setOnCameraChangeListener { cameraPosition ->

            preferences
                .edit()
                .putFloat("lat", cameraPosition.target.latitude.toFloat())
                .apply()

            preferences
                .edit()
                .putFloat("lng", cameraPosition.target.longitude.toFloat())
                .apply()

            Timber.d("lat %s", cameraPosition.target.latitude)
            Timber.d("lng %s", cameraPosition.target.longitude)
        }

    }

    private fun checkPermissions() {
        Dexter.withContext(navigationActivity)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                @SuppressLint("MissingPermission")
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        getMap()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {

                }
            }).check()
    }

    private fun getMap() {
        val fragment = SupportMapFragment.newInstance()
        fragment.getMapAsync(this)
        childFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .commit()
    }

    private fun setMarkers() {
        googleMap.clear()
        clusterManager = ClusterManager(navigationActivity, googleMap)

        val markersList = soilModel.cls_Project.Profil?.cls_Prof
        Timber.d("Markers list: %s", markersList?.toJson() ?: null)
        if (markersList.isNullOrEmpty()) {
            val lat = preferences.getFloat("lat", 0f).toDouble()
            val lng = preferences.getFloat("lng", 0f).toDouble()
            if (lat != 0.0 && lng != 0.0) {
                googleMap.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            lat,
                            lng
                        ),
                        5f
                    )
                )
            }
            return
        }


        for (i in markersList.indices) {
            for (j in markersList.indices) {
                if (i == j)
                    continue

                val aLat = markersList[i].Y
                val aLng = markersList[i].X
                val bLat = markersList[j].Y
                val bLNg = markersList[j].X

                if (aLat != null && aLat != 0.0 &&
                    aLng != null && aLng != 0.0 &&
                    bLat != null && bLat != 0.0 &&
                    bLNg != null && bLNg != 0.0
                ) {
                    if (aLat == bLat && aLng == bLNg) {
                        val newLat = aLat + (Math.random() - .5) / 1500
                        val newLng = aLng + (Math.random() - .5) / 1500
                        markersList[i].Y = newLat
                        markersList[i].X = newLng
                    }
                }
            }
        }

        if (markersList.isNullOrEmpty()) {
            return
        }


        var cameraMoved = false
        markersList.forEach { marker ->
            val latitude = marker.Y
            val longitude = marker.X
            if (latitude != null && longitude != null) {
                clusterManager.addItem(Group(latitude.toDouble(), longitude.toDouble(), marker.Nr))

                if (!cameraMoved && latitude != 0.0 && longitude != 0.0) {
                    cameraMoved = true
                    googleMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                latitude,
                                longitude
                            ),
                            zoomLevel
                        )
                    )
                }
            }
        }
        val clusterRenderer = MarkerRender(
            activity,
            googleMap,
            clusterManager
        )

        clusterManager.setOnClusterClickListener {
            zoomLevel = googleMap.cameraPosition.zoom + 2f

            googleMap.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    it.position,
                    zoomLevel
                )
            )
            true
        }
        clusterManager.renderer = clusterRenderer
        clusterManager.setOnClusterItemClickListener { item: Group? ->
            if (item != null) {
                for (i in markersList.indices) {
                    if (item.position.latitude == markersList[i].Y &&
                        item.position.longitude == markersList[i].X
                    ) {
                        navigationActivity.openProfileDetails(markersList[i].Nr)
                        break
                    }
                }
            }
            true
        }
        googleMap.setOnCameraIdleListener(clusterManager)
        googleMap.setOnMarkerClickListener(clusterManager)
    }
}                                                                                                                                                                                                                                                 