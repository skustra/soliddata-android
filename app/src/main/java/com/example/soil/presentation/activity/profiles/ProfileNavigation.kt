package com.example.soil.presentation.activity.profiles

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.soil.model.cls.SoilModel

sealed class FileNavigation {
    object ExportFile : FileNavigation()
    object ClsMapScreen : FileNavigation()
    object ClsProfilesScreen : FileNavigation()
    data class ClsProfileDetails(val soilModel: SoilModel, val number: String) : FileNavigation()

}

class FileNavigationViewModel : ViewModel() {

    private val navigation: MutableLiveData<FileNavigation> = MutableLiveData(
        FileNavigation.ClsMapScreen
    )

    fun getNavigation(): LiveData<FileNavigation> = navigation

    fun changeNavigation(navigation: FileNavigation) {
        this.navigation.value = navigation
    }

    fun addProfile() {

    }

    fun removeProfile(profileNumber: String) {

    }
}