package com.example.soil.presentation.fragment.ground.details

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.example.soil.R
import com.example.soil.model.cls.ClsProf
import com.example.soil.model.cls.VarGrunt
import com.example.soil.presentation.activity.ground.EditGroundLayersActivity
import com.example.soil.presentation.activity.ground.Field
import com.example.soil.presentation.activity.ground.Navigation
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.utils.*
import kotlinx.android.synthetic.main.fragment_ground_layer_details.*
import timber.log.Timber
import utils.hide
import utils.isVisible
import utils.round
import utils.show

class GroundLayerDetailsFragment : BaseFragment() {

    private var groundIndex: Int = 0

    private lateinit var groundLayersActivity: EditGroundLayersActivity

    companion object {
        fun getInstance(
            groundIndex: Int,
        ): GroundLayerDetailsFragment {
            return GroundLayerDetailsFragment().apply {
                arguments = Bundle().apply {
                    putInt(Constants.GROUND_INDEX, groundIndex)
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        groundLayersActivity = context as EditGroundLayersActivity
    }

    override fun setLayout() = R.layout.fragment_ground_layer_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        groundIndex = arguments?.getInt(Constants.GROUND_INDEX) as Int
        position.text = (groundIndex + 1).toString()

        if (groundLayersActivity.viewModel.isEuroNorm()) {
            g1Text.text = "Składowa pierwszorzędna"
            g2Text.text = "Składowa drugorzędna"
            g3Text.text = "Składowa trzeciorzędna"
        } else {
            g1Text.text = "Grunt"
            g2Text.text = "Przewarstwienie"
            g3Text.text = "Dodatki"
        }

        groundLayersActivity.viewModel.getClsProfile().listen(viewLifecycleOwner) { clsProfile ->
            Timber.w("%s", clsProfile.toJson())
            val varGround = clsProfile.warstwy.var_grunt[groundIndex]

            setThicknessValue(varGround, clsProfile)

            fullGroundName.text = varGround.POpisWarstwy
            setBackground(fullGroundNameContainer, fullGroundName.text.isEmpty())

            setG1(varGround)
            setG2(varGround)
            setG3(varGround)

            setHumidityValue(varGround)
            setState(varGround)
            setRollingNumber(varGround)
            setColour(varGround)

            thicknessSumContainer.setSafeOnClickListener {
                val thicknessOneBeforeLast =
                    if (groundIndex > 0) clsProfile.warstwy.var_grunt[groundIndex - 1].PMiazszosc.mm
                    else 0.0
                edit(
                    Field.ThicknessSum(
                        thicknessOneBeforeLast,
                        varGround.PMiazszosc.mm,
                        getThicknessSumMillimetre(varGround, clsProfile)
                    )
                )
            }
        }

        groundLayersActivity.viewModel.getStatesLiveData()
            .listen(viewLifecycleOwner) { states ->
                if (states.size > 1) {
                    stateContainer.alpha = 1f
                    stateContainer.isEnabled = true
                } else {
                    stateContainer.alpha = 0.5f
                    stateContainer.isEnabled = false
                    groundLayersActivity.viewModel.setDefaultState(groundIndex)
                }
            }

        groundLayersActivity.viewModel.getRollingsLiveData()
            .listen(viewLifecycleOwner) { rollings ->
                if (rollings.size > 1) {
                    rollingsNumberContainer.alpha = 1f
                    rollingsNumberContainer.isEnabled = true
                } else {
                    rollingsNumberContainer.alpha = 0.5f
                    rollingsNumberContainer.isEnabled = false
                    groundLayersActivity.viewModel.setDefaultRollings(groundIndex)
                }
            }

        thicknessContainer.setSafeOnClickListener {
            edit(Field.Thickness)
        }

        commentsContainer.setSafeOnClickListener {
            edit(Field.Comment)
        }

        groundContainer.setSafeOnClickListener {
            edit(Field.G1)
        }

        fullGroundNameContainer.setSafeOnClickListener {
            edit(Field.GroundName)
        }

        humidityContainer.setSafeOnClickListener {
            edit(Field.Humidity)
        }

        stateContainer.setSafeOnClickListener {
            edit(Field.State)
        }

        rollingsNumberContainer.setSafeOnClickListener {
            edit(Field.RollingsNumber)
        }

        colourContainer.setSafeOnClickListener {
            edit(Field.Colour)
        }

        groundContainer.setSafeOnClickListener {
            if (groundListContainer.isVisible()) {
                groundListContainer.hide()
                arrow.rotation = 90f
            } else {
                groundListContainer.show()
                arrow.rotation = 270f
            }
        }

        g1Container.setSafeOnClickListener {
            edit(Field.G1)
        }

        g2Container.setSafeOnClickListener {
            edit(Field.G2)
        }

        g3Container.setSafeOnClickListener {
            edit(Field.G3)
        }
    }

    private fun setThicknessValue(
        varGround: VarGrunt,
        clsProfile: ClsProf
    ) {
        thickness.text = "${varGround.PMiazszosc.mm.millimetreToMeter().round(2)} m"

        thicknessSum.text = "${getThicknessSumMillimetre(varGround, clsProfile)
            .millimetreToMeter()
            .round(2)} m ppt"
        setBackground(thicknessContainer, thickness.text.isEmpty())
        setBackground(thicknessSumContainer, thicknessSum.text.isEmpty())
    }

    private fun getThicknessSumMillimetre(
        varGround: VarGrunt,
        clsProfile: ClsProf
    ): Double {
        val thicknessOneBeforeLast =
            if (groundIndex > 0) clsProfile.warstwy.var_grunt[groundIndex - 1].PMiazszosc.mm
            else 0.0
        val thicknessValue = varGround.PMiazszosc.mm
        return thicknessValue + thicknessOneBeforeLast
    }

    private fun setG1(varGround: VarGrunt) {
        val displayValue = groundLayersActivity.viewModel.findG1Ground(varGround.G1)
        Timber.d("displayValue %s | g1Value text: %s",displayValue, g1Value.text)
        if (g1Value.text != displayValue) {
            Timber.d("diff")
            g1Value.text = displayValue
            groundLayersActivity.viewModel.fetchStates(varGround.G1)
            groundLayersActivity.viewModel.fetchRollings(varGround.G1)
            groundLayersActivity.viewModel.fetchGroundName(varGround, groundIndex)
        }
        setBackground(groundContainer, g1Value.text.isEmpty())
    }

    private fun setG2(varGround: VarGrunt) {
        val displayValue = groundLayersActivity.viewModel.findG2Ground(varGround.G2)
        if (g2Value.text != displayValue) {
            groundLayersActivity.viewModel.fetchGroundName(varGround, groundIndex)
            g2Value.text = displayValue
        }
    }

    private fun setG3(varGround: VarGrunt) {
        val displayValue = groundLayersActivity.viewModel.findG3Ground(varGround.G3)
        if (g3Value.text != displayValue) {
            groundLayersActivity.viewModel.fetchGroundName(varGround, groundIndex)
            g3Value.text = displayValue
        }
    }

    private fun setState(varGround: VarGrunt) {
        state.text = groundLayersActivity
            .viewModel
            .getStates()
            .firstOrNull { it.stan == varGround.xml_Stan }?.stanik_Pelna ?: "nn"
        setBackground(stateContainer, state.text.isEmpty())
    }

    private fun setHumidityValue(varGround: VarGrunt) {
        humidity.text = groundLayersActivity
            .viewModel
            .getHumidity()
            .firstOrNull { it.wilg == varGround.PWilgot.wilg }?.pelnaNazwa ?: "nn"
        setBackground(humidityContainer, humidity.text.isEmpty())
    }

    private fun setRollingNumber(varGround: VarGrunt) {
        rollerNumber.text = groundLayersActivity.viewModel.getRollings()
            .firstOrNull { it.lWal == varGround.PNOR.LWal }?.str ?: "nn"
        setBackground(rollingsNumberContainer, rollerNumber.text.isEmpty())
    }

    private fun setColour(varGround: VarGrunt) {
        colour.text = varGround.PColour
        setBackground(colourContainer, colour.text.isEmpty())
    }

    private fun edit(field: Field) {
        groundLayersActivity
            .viewModel
            .changeNavigation(Navigation.EditGround(field, groundIndex))
    }

    private fun setBackground(layout: LinearLayout, isEmpty: Boolean) {
        if (isEmpty && layout.isEnabled) {
            layout.setBackgroundColor(ContextCompat.getColor(groundLayersActivity, R.color.yellow))
        } else {
            layout.setBackgroundColor(ContextCompat.getColor(groundLayersActivity, R.color.white))
        }
    }
}