package com.example.soil.presentation.activity.ground

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.example.soil.R
import com.example.soil.model.cls.SoilModel
import com.example.soil.presentation.activity.BaseActivity
import com.example.soil.presentation.activity.water.EditWaterLayersActivity
import com.example.soil.presentation.fragment.ground.details.GroundLayerDetailsFragment
import com.example.soil.presentation.fragment.ground.edit.EditGroundFragment
import com.example.soil.presentation.fragment.ground.header.LayerHeaderFragment
import com.example.soil.presentation.fragment.ground.layers.GroundLayersFragment
import com.example.soil.presentation.fragment.profiles.edit.EditClsProfileFragment
import com.example.soil.utils.Constants.PROFILE_NUMBER
import com.example.soil.utils.Constants.SOIL_MODEL
import com.example.soil.utils.RequestCodes
import com.example.soil.utils.addFragmentWithBackStack
import com.example.soil.utils.listen
import com.example.soil.utils.replaceFragment
import kotlinx.android.synthetic.main.activity_edit_layer.*
import timber.log.Timber
import utils.shouldBeVisibleIf
import utils.show

class EditGroundLayersActivity : BaseActivity() {

    val viewModel: EditGroundLayerViewModel by lazy {
        provideViewModel(EditGroundLayerViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_layer)

        backArrow.setOnClickListener {
            onBackPressed()
        }

        val profileNumber = intent.getStringExtra(PROFILE_NUMBER) ?: return
        viewModel.setData(intent.getParcelableExtra<SoilModel>(SOIL_MODEL)!!, profileNumber)

        viewModel.getNavigation().listen(this) { navigation ->

            progressBar.shouldBeVisibleIf(navigation is Navigation.Loading)

            when (navigation) {
                is Navigation.GroundLayersList -> {
                    headerFragmentContainer.show()
                    replaceFragment(GroundLayersFragment(), R.id.contentFragmentContainer)
                }
                is Navigation.GroundLayerDetails -> {
                    addFragmentWithBackStack(
                        GroundLayerDetailsFragment.getInstance(navigation.groundIndex),
                        R.id.contentFragmentContainer
                    )
                }
                is Navigation.EditGround -> {
                    addFragmentWithBackStack(
                        EditGroundFragment.getInstance(
                            navigation.field,
                            navigation.groundIndex
                        ),
                        R.id.contentFragmentContainer
                    )
                }

                is Navigation.WaterLayers -> {
                    startActivityForResult(
                        Intent(this, EditWaterLayersActivity::class.java).apply {
                            putExtra(SOIL_MODEL, viewModel.getUpdatedSoilModel())
                            putExtra(PROFILE_NUMBER, profileNumber)
                        }, RequestCodes.EDIT_FILE_CODE
                    )
                }

                is Navigation.EditClsProfile -> {
                    addFragmentWithBackStack(
                        EditClsProfileFragment.getInstance(
                            navigation.field,
                        ),
                        R.id.contentFragmentContainer
                    )
                }

                is Navigation.Loading -> {

                }
            }
        }
        replaceFragment(LayerHeaderFragment(), R.id.headerFragmentContainer)
        viewModel.fetchSoilData()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.d("onActivityResult %s %s", requestCode, resultCode)
        if (requestCode == RequestCodes.EDIT_FILE_CODE && resultCode == Activity.RESULT_OK) {
            val soilModel = data?.getParcelableExtra<SoilModel>(SOIL_MODEL) as SoilModel
            viewModel.updateSoilModel(soilModel)
        }
    }

    override fun finish() {
        val soilModel = viewModel.getUpdatedSoilModel()
        if (soilModel != null) {
            val data = Intent()
                .apply {
                    putExtras(Bundle().apply {
                        putParcelable(SOIL_MODEL, soilModel)
                    })
                }
            setResult(Activity.RESULT_OK, data)
        }
        super.finish()
    }
}