package com.example.soil.presentation.activity.water


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.soil.model.cls.*
import com.example.soil.usecase.MockSoilModel
import com.example.soil.utils.toJson
import timber.log.Timber
import java.io.Serializable

sealed class Field : Serializable {

    data class EditWater(var position: Int) : Field()
    data class EditFiltration(var position: Int) : Field()
    data class EditSamples(var position: Int) : Field()
    object WaterLayersDetails : Field()
}

class EditWaterLayersViewModel : ViewModel() {

    private lateinit var soilModel: SoilModel

    private val navigation: MutableLiveData<Field> =
        MutableLiveData(Field.WaterLayersDetails)

    fun getNavigation(): LiveData<Field> = navigation

    fun changeNavigation(field: Field) {
        this.navigation.value = field
    }

    private val clsProfileLiveData: MutableLiveData<ClsProf> = MutableLiveData()

    fun getClsProfile(): LiveData<ClsProf> = clsProfileLiveData

    fun setClsProfile(clsProf: ClsProf) {
        Timber.d("Set cls profile: %s", clsProf.toJson())
        this.clsProfileLiveData.value = clsProf
    }

    fun setData(soilModel: SoilModel, profileNumber: String) {

        soilModel.cls_Project.Profil?.cls_Prof?.forEach {
            if (it.Nr == profileNumber) {
                setClsProfile(it)
            }
        }

        this.soilModel = soilModel
    }

    fun getUpdatedSoilModel(): SoilModel? {
        Timber.d("getUpdatedSoilModel")
        if (::soilModel.isInitialized) {

            val profile = getClsProfile().value ?: return soilModel
            val clsProfileList = soilModel.cls_Project.Profil?.cls_Prof ?: return soilModel

            for (i in clsProfileList.indices) {
                Timber.d(
                    "Cls profile was updated %s %s %s",
                    i,
                    clsProfileList[i].Nr,
                    profile.Nr
                )

                if (clsProfileList[i].Nr == profile.Nr) {
                    Timber.d("Updated cls profile water %s", profile.wody?.toJson())
                    clsProfileList[i] = profile
                    break
                }
            }
            Timber.d(
                "Updated  2 cls profile water %s",
                soilModel.cls_Project?.Profil?.cls_Prof?.toJson()
            )
            return soilModel
        } else return null
    }

    fun addWater() {
        val profile = getClsProfile().value ?: return
        if (profile.wody == null)
            profile.wody = Wody(arrayListOf())

        profile.wody?.var_wod?.add(MockSoilModel().getWater())
        clsProfileLiveData.value = profile
    }

    fun addFiltration() {
        val profile = getClsProfile().value ?: return
        if (profile.saczenia == null)
            profile.saczenia = Saczenia(arrayListOf())

        profile.saczenia?.var_sacz?.add(MockSoilModel().getFiltration())
        clsProfileLiveData.value = profile
    }

    fun addSample() {
        val profile = getClsProfile().value ?: return
        if (profile.Samples == null)
            profile.Samples = Samples(arrayListOf())

        profile.Samples?.cls_Sample?.add(
            MockSoilModel().getClsSample()
        )
        clsProfileLiveData.value = profile
    }

    fun removeItem(field: Field) {
        val profile = getClsProfile().value ?: return
        when (field) {
            is Field.EditWater -> {
                profile.wody?.var_wod?.removeAt(field.position)
            }

            is Field.EditFiltration -> {
                profile.saczenia?.var_sacz?.removeAt(field.position)

            }

            is Field.EditSamples -> {
                profile.Samples?.cls_Sample?.removeAt(field.position)
            }
        }
        clsProfileLiveData.value = profile
    }
}