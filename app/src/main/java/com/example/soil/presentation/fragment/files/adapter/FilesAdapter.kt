package com.example.soil.presentation.fragment.files.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.soil.R
import com.example.soil.model.cls.ClsProf
import com.example.soil.utils.millimetreToMeter
import java.io.File
import kotlin.reflect.KFunction1

class FilesAdapter(
    val data: Array<File>,
    val pickFile: KFunction1<File, Unit>,
) :
    RecyclerView.Adapter<FilesAdapter.LayersViewHolder>() {

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LayersViewHolder {
        return LayersViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_file, parent, false)
        )
    }

    override fun onBindViewHolder(holder: LayersViewHolder, position: Int) {

        val item = data[position]

        holder.fileName.text = item.name
        holder.filePath.text = item.absolutePath
        holder.fileName.setOnClickListener {
            pickFile(item)
        }
    }

    class LayersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val fileName: TextView = view.findViewById(R.id.fileName)
        val filePath: TextView = view.findViewById(R.id.filePath)
    }
}