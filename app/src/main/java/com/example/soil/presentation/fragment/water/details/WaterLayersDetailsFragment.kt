package com.example.soil.presentation.fragment.water.details

import android.content.Context
import android.os.Bundle
import android.view.View
import com.example.soil.R
import com.example.soil.presentation.activity.water.EditWaterLayersActivity
import com.example.soil.presentation.activity.water.Field
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.presentation.fragment.water.details.adapter.FiltrationLayersAdapter
import com.example.soil.presentation.fragment.water.details.adapter.SampleLayersAdapter
import com.example.soil.presentation.fragment.water.details.adapter.WaterLayersAdapter
import com.example.soil.utils.listen
import kotlinx.android.synthetic.main.fragment_water_layer_details.*

class WaterLayersDetailsFragment : BaseFragment() {


    private lateinit var editWaterLayersActivity: EditWaterLayersActivity

    companion object {
        fun getInstance(): WaterLayersDetailsFragment {
            return WaterLayersDetailsFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        editWaterLayersActivity = context as EditWaterLayersActivity
    }

    override fun setLayout() = R.layout.fragment_water_layer_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editWaterLayersActivity.viewModel.getClsProfile().listen(viewLifecycleOwner) { clsProfile ->
            waterLayers.adapter = WaterLayersAdapter(
                clsProfile.wody?.var_wod ?: arrayListOf(),
                ::editWater,
                ::removeWater
            )
            filtrationLayers.adapter = FiltrationLayersAdapter(
                clsProfile.saczenia?.var_sacz ?: arrayListOf(),
                ::editFiltration,
                ::removeFiltration
            )
            samplesLayers.adapter = SampleLayersAdapter(
                clsProfile.Samples?.cls_Sample ?: arrayListOf(),
                ::editSample,
                ::removeSample
            )
        }

        waterContainer.setOnClickListener {
            editWaterLayersActivity.viewModel.addWater()
        }
        filtrationContainer.setOnClickListener {
            editWaterLayersActivity.viewModel.addFiltration()
        }
        samplesContainer.setOnClickListener {
            editWaterLayersActivity.viewModel.addSample()
        }
    }

    private fun editWater(position: Int) {
        edit(Field.EditWater(position))
    }

    private fun editFiltration(position: Int) {
        edit(Field.EditFiltration(position))
    }

    private fun editSample(position: Int) {
        edit(Field.EditSamples(position))
    }

    private fun removeWater(position: Int) {
        editWaterLayersActivity.remove(Field.EditWater(position))
    }

    private fun removeFiltration(position: Int) {
        editWaterLayersActivity.remove(Field.EditFiltration(position))
    }

    private fun removeSample(position: Int) {
        editWaterLayersActivity.remove(Field.EditSamples(position))
    }

    private fun edit(field: Field) {
        editWaterLayersActivity
            .viewModel
            .changeNavigation(field)
    }
}