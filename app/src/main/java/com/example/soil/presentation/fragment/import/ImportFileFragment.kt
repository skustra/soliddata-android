package com.example.soil.presentation.fragment.import

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.example.soil.R
import com.example.soil.model.cls.SoilModel
import com.example.soil.presentation.activity.import.ImportFileActivity
import com.example.soil.presentation.activity.import.Navigation
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.usecase.ImportSoilModelUseCase
import com.example.soil.usecase.MockSoilModel
import com.example.soil.utils.RequestCodes
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.fragment_import_file.*


class ImportFileFragment : BaseFragment() {

    private lateinit var importFileActivity: ImportFileActivity

    override fun setLayout() = R.layout.fragment_import_file

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        newProject.setOnClickListener {
            showEuroNormDialog()
        }

        importFile.setOnClickListener {
            importFileActivity.viewModel.changeNavigation(Navigation.FilesScreen)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        importFileActivity = context as ImportFileActivity
    }

    private fun showEuroNormDialog() {
        AlertDialog.Builder(importFileActivity)
            .setSingleChoiceItems(
                arrayOf(
                    getString(R.string.pl_norm),
                    getString(R.string.eur_norm)
                ), 0, null
            )
            .setTitle(R.string.pick_norm)
            .setPositiveButton(
                R.string.ok
            ) { dialog, _ ->
                dialog.dismiss()
                val euroNorm: Boolean =
                    (dialog as AlertDialog)
                        .listView.checkedItemPosition == 1
                showProjectNameDialog(euroNorm)
            }
            .show()
    }

    private fun showProjectNameDialog(euroNorm: Boolean) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.type_project_name))
        val viewInflated: View = LayoutInflater.from(context)
            .inflate(R.layout.item_input_project_name, view as ViewGroup?, false)
        val input = viewInflated.findViewById<View>(R.id.input) as EditText
        builder.setView(viewInflated)
        builder.setCancelable(false)
        builder.setPositiveButton(
            android.R.string.ok
        ) { dialog, _ ->
            if (input.text.isNotEmpty()) {
                dialog.dismiss()
                importFileActivity.viewModel.changeNavigation(
                    Navigation.FileNavigationScreen(
                        MockSoilModel()
                            .getSoilModel(euroNorm, input.text.toString())
                    )
                )
            } else {
                Toast.makeText(activity, getString(R.string.type_project_name), Toast.LENGTH_LONG)
                    .show()
            }
        }
        builder.setNegativeButton(
            android.R.string.cancel
        ) { dialog, _ -> dialog.cancel() }

        builder.show()
    }
}