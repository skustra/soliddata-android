package com.example.soil.presentation.fragment.profiles.edit

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.View
import com.example.soil.R
import com.example.soil.model.cls.ClsProf
import com.example.soil.presentation.activity.ground.EditGroundLayersActivity
import com.example.soil.presentation.activity.ground.Field
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.utils.*
import com.example.soil.utils.Constants.FIELD
import kotlinx.android.synthetic.main.fragment_value_editor.*
import timber.log.Timber
import utils.afterTextChanged
import utils.hide
import utils.show
import java.text.SimpleDateFormat
import java.util.*

class EditClsProfileFragment : BaseFragment() {

    private lateinit var field: Field
    private lateinit var clsProfile: ClsProf
    private lateinit var groundLayersActivity: EditGroundLayersActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        groundLayersActivity = context as EditGroundLayersActivity
    }

    companion object {
        fun getInstance(field: Field): EditClsProfileFragment {
            return EditClsProfileFragment()
                .apply {
                    arguments = Bundle().apply {
                        putSerializable(FIELD, field)
                    }
                }
        }
    }

    override fun setLayout() = R.layout.fragment_value_editor

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        container.setOnClickListener { }
        clsProfile = getClsProfile() ?: return

        saveButton.setSafeOnClickListener {
            groundLayersActivity.hideKeyboard(view)
            groundLayersActivity.viewModel.setClsProfile(clsProfile)
            groundLayersActivity.onBackPressed()
        }

        field = arguments?.getSerializable(FIELD) as Field
        setEditorUi()
        setInputChangedListener()
    }

    private fun setEditorUi() {
        when (field) {
            is Field.Latitude -> {
                val profile = getClsProfile()
                if (profile != null) {
                    firstValueEditText.apply {
                        inputType =
                            (InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL)

                        val value = profile.Y?.toString()

                        if (value.isNullOrEmpty() || value == "0.0") {
                            setText("")
                            hint = "0.0"
                        } else {
                            setText(value)
                        }

                        show()
                    }
                }
            }
            is Field.Longitude -> {
                val profile = getClsProfile()
                if (profile != null) {
                    firstValueEditText.apply {
                        inputType =
                            InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                        val value = profile.X?.toString()

                        if (value.isNullOrEmpty() || value == "0.0") {
                            setText("")
                            hint = "0.0"
                        } else {
                            setText(value)
                        }

                        show()
                    }
                }
            }
            is Field.OrdinateOfMesurement -> {
                val profile = getClsProfile()
                if (profile != null) {
                    firstValueEditText.apply {
                        inputType =
                            InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                        val value = profile.RzednaOtworu.mm.millimetreToMeter()

                        if (value == 0.0) {
                            setText("")
                            hint = "0.0"
                        } else {
                            setText(value.toString())
                        }
                        show()
                    }
                }
            }
            is Field.Date -> {
                firstValueEditText.hide()
                dateTextView.show()
                dateTextView.text = DateUtils.formatDate(clsProfile.SondaData ?: "")
                dateTextView.setOnClickListener {
                    val calendar = Calendar.getInstance()

                    val date = SimpleDateFormat(
                        DateUtils.XML_SOURCE_DATE_FORMAT, Locale.getDefault()
                    ).parse(clsProfile.SondaData)

                    if (date == null) {
                        groundLayersActivity.onBackPressed()
                        return@setOnClickListener
                    }

                    calendar.time = date

                    val listener = OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        calendar.apply {
                            set(Calendar.YEAR, year)
                            set(Calendar.MONTH, monthOfYear)
                            set(Calendar.DAY_OF_MONTH, dayOfMonth)
                            showTimePickerDialog(calendar)
                        }
                    }

                    DatePickerDialog(
                        groundLayersActivity,
                        listener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                    ).show()
                }
            }
            else -> {
                firstValueEditText.hide()
            }
        }
    }

    private fun setInputChangedListener() {

        firstValueEditText.afterTextChanged { value ->
            Timber.d("value %s", value)
            when (field) {
                is Field.Latitude -> {
                    val profile = getClsProfile()
                    if (profile != null) {
                        profile.Y = getDouble(value)
                    }
                }

                is Field.Longitude -> {
                    val profile = getClsProfile()
                    if (profile != null) {
                        profile.X = getDouble(value)
                    }
                }
                is Field.OrdinateOfMesurement -> {
                    val profile = getClsProfile()
                    if (profile != null) {
                        profile.RzednaOtworu.mm = getDouble(value).meterToMillimetre()
                    }
                }
                else -> {

                }
            }
        }
    }

    private fun getClsProfile() = groundLayersActivity
        .viewModel
        .getClsProfile()
        .value

    private fun getDouble(value: String): Double {
        try {
            return value.toDouble()
        } catch (e: NumberFormatException) {
            Timber.e(e)
        }
        return 0.0
    }

    private fun showTimePickerDialog(calendar: Calendar) {
        TimePickerDialog(
            groundLayersActivity,
            TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->

                calendar.apply {
                    set(Calendar.HOUR_OF_DAY, hourOfDay)
                    set(Calendar.MINUTE, minute)
                }

                clsProfile.SondaData = SimpleDateFormat(
                    DateUtils.XML_SOURCE_DATE_FORMAT_SAVE,
                    Locale.getDefault()
                ).format(calendar.time)

                clsProfile.Data = clsProfile.SondaData

                dateTextView.text = DateUtils.formatDate(clsProfile.SondaData ?: "")

            },
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE), true
        ).show()
    }
}
