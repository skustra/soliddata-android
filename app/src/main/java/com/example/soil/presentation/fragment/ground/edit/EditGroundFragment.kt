package com.example.soil.presentation.fragment.ground.edit

import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.View
import com.example.soil.R
import com.example.soil.model.cls.VarGrunt
import com.example.soil.model.value.Value
import com.example.soil.presentation.activity.ground.EditGroundLayersActivity
import com.example.soil.presentation.activity.ground.Field
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.presentation.fragment.ground.edit.adapter.ValuesAdapter
import com.example.soil.utils.*
import com.example.soil.utils.Constants.FIELD
import com.example.soil.utils.Constants.GROUND_INDEX
import kotlinx.android.synthetic.main.fragment_value_editor.*
import timber.log.Timber
import utils.afterTextChanged
import utils.hide
import utils.round
import utils.show

class EditGroundFragment : BaseFragment() {

    private lateinit var field: Field
    private var groundIndex: Int = 0
    private lateinit var ground: VarGrunt
    private lateinit var valuesAdapter: ValuesAdapter
    private var euroNorm = false

    private lateinit var groundLayersActivity: EditGroundLayersActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        groundLayersActivity = context as EditGroundLayersActivity
    }

    companion object {
        fun getInstance(field: Field, groundIndex: Int): EditGroundFragment {
            return EditGroundFragment().apply {
                arguments = Bundle().apply {
                    putInt(GROUND_INDEX, groundIndex)
                    putSerializable(FIELD, field)
                }
            }
        }
    }

    override fun setLayout() = R.layout.fragment_value_editor

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        container.setOnClickListener { }
        euroNorm = groundLayersActivity.viewModel.isEuroNorm()

        groundIndex = arguments?.getInt(Constants.GROUND_INDEX) as Int
        ground = groundLayersActivity.viewModel.getGround(groundIndex) ?: return

        saveButton.setSafeOnClickListener {
            groundLayersActivity.hideKeyboard(view)
            groundLayersActivity.viewModel.updateEditedGround(groundIndex, ground)
            groundLayersActivity.onBackPressed()
        }

        field = arguments?.getSerializable(FIELD) as Field
        setEditorUi()
        setInputChangedListener()
    }

    private fun pickValue(value: Value) {
        Timber.d("picked: %s", value.toJson())
        when (field) {
            is Field.Humidity -> {
                ground.PWilgot.wilg = value.id
                ground.wilg_e = value.id
            }
            is Field.State -> {
                ground.xml_Stan = value.id
            }

            is Field.G1 -> {
                ground.G1 = value.id
            }
            is Field.G2 -> {
                ground.G2 = value.id
            }
            is Field.G3 -> {
                ground.G3 = value.id
            }

            is Field.RollingsNumber -> {
                ground.NOR_e = value.id
                ground.PNOR.LWal = value.id
            }
        }
    }

    private fun setEditorUi() {
        when (field) {

            is Field.Colour -> {
                firstValueEditText.apply {
                    inputType = InputType.TYPE_CLASS_TEXT
                    setText(ground.PColour)
                    show()
                }
            }

            is Field.GroundName -> {
                firstValueEditText.apply {
                    inputType = InputType.TYPE_CLASS_TEXT
                    setText(ground.POpisWarstwy ?: "")
                    show()
                }
            }

            is Field.RollingsNumber -> {
                valuesRecyclerView.show()
                valuesAdapter =
                    ValuesAdapter(groundLayersActivity.viewModel.getRollings().map {
                        Value(
                            id = it.lWal,
                            displayValue = it.str,
                            selected = it.lWal == ground.NOR_e
                        )
                    }, ::pickValue)
                valuesRecyclerView.adapter = valuesAdapter
            }

            is Field.G1 -> {
                valuesRecyclerView.show()
                valuesAdapter =
                    ValuesAdapter(groundLayersActivity.viewModel.getG1().map {
                        Value(
                            id = it.frak,
                            displayValue = it.nazwaGlowna,
                            selected = it.frak == ground.G1
                        )
                    }, ::pickValue)
                valuesRecyclerView.adapter = valuesAdapter
            }

            is Field.G2 -> {
                valuesRecyclerView.show()
                valuesAdapter =
                    ValuesAdapter(groundLayersActivity.viewModel.getG2().map {
                        Value(
                            id = it.frak,
                            displayValue = it.nazwaGlowna,
                            selected = it.frak == ground.G2
                        )
                    }, ::pickValue)
                valuesRecyclerView.adapter = valuesAdapter
            }

            is Field.G3 -> {
                valuesRecyclerView.show()
                valuesAdapter =
                    ValuesAdapter(groundLayersActivity.viewModel.getG3().map {
                        Value(
                            id = it.frak,
                            displayValue = it.nazwaGlowna,
                            selected = it.frak == ground.G3
                        )
                    }, ::pickValue)
                valuesRecyclerView.adapter = valuesAdapter
            }

            is Field.Thickness -> {
                firstValueEditText.apply {
                    inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                    val value = ground.PMiazszosc.mm.millimetreToMeter()

                    if (value == 0.0) {
                        setText("")
                        hint = "0.0"
                    } else {
                        setText(value.toString())
                    }

                    show()
                }
            }
            is Field.ThicknessSum -> {
                firstValueEditText.apply {
                    inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                    val value =
                        "${
                        (field as Field.ThicknessSum).thicknessSum.millimetreToMeter().round(2)}"


                    if (value.isEmpty() || value == "0.0") {
                        setText("")
                        hint = "0.0"
                    } else {
                        setText(value)
                    }

                    show()
                }
            }

            is Field.Humidity -> {
                valuesRecyclerView.show()
                valuesAdapter =
                    ValuesAdapter(groundLayersActivity.viewModel.getHumidity().map {
                        Value(
                            id = it.wilg,
                            displayValue = it.pelnaNazwa,
                            selected = it.wilg == ground.PWilgot.wilg
                        )
                    }, ::pickValue)
                valuesRecyclerView.adapter = valuesAdapter
            }
            is Field.State -> {
                valuesRecyclerView.show()
                valuesAdapter =
                    ValuesAdapter(groundLayersActivity.viewModel.getStates().map {
                        Value(
                            id = it.stan,
                            displayValue = it.stanik_Pelna,
                            selected = it.stan == ground.xml_Stan
                        )
                    }, ::pickValue)

                valuesRecyclerView.adapter = valuesAdapter
            }
            else -> {
                firstValueEditText.hide()
                valuesRecyclerView.hide()
            }
        }
    }

    private fun setInputChangedListener() {

        firstValueEditText.afterTextChanged { value ->
            Timber.d("value %s", value)
            when (field) {
                is Field.GroundName -> {
                    ground.POpisWarstwy = value
                }

                is Field.Thickness -> {
                    ground.PMiazszosc.mm = getDouble(value).meterToMillimetre()
                }

                is Field.Colour -> {
                    ground.PColour = value
                }

                is Field.ThicknessSum -> {
                    val thicknessField = field as Field.ThicknessSum

                    val thicknessSum = getDouble(value).meterToMillimetre()
                    val result =
                        thicknessField.thickness + (thicknessSum - thicknessField.thicknessSum)
                    ground.PMiazszosc.mm = if (result >= 0.0)
                        result else 0.0
                }
                else -> {
                }
            }
        }
    }

    private fun getDouble(value: String): Double {
        try {
            return value.toDouble()
        } catch (e: NumberFormatException) {
            Timber.e(e)
        }
        return 0.0
    }
}