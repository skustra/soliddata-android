package com.example.soil.presentation.fragment.water.edit

import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.View
import com.example.soil.R
import com.example.soil.model.cls.ClsProf
import com.example.soil.presentation.activity.water.EditWaterLayersActivity
import com.example.soil.presentation.activity.water.Field
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.utils.Constants.FIELD
import com.example.soil.utils.hideKeyboard
import com.example.soil.utils.meterToMillimetre
import com.example.soil.utils.millimetreToMeter
import com.example.soil.utils.setSafeOnClickListener
import kotlinx.android.synthetic.main.fragment_value_editor.container
import kotlinx.android.synthetic.main.fragment_value_editor.firstValueEditText
import kotlinx.android.synthetic.main.fragment_water_layer_editor.*
import timber.log.Timber
import utils.afterTextChanged
import utils.hide

class EditWaterLayersFragment : BaseFragment() {

    private lateinit var field: Field
    private lateinit var clsProfile: ClsProf
    private lateinit var groundLayersActivity: EditWaterLayersActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        groundLayersActivity = context as EditWaterLayersActivity
    }

    companion object {
        fun getInstance(field: Field): EditWaterLayersFragment {
            return EditWaterLayersFragment()
                .apply {
                    arguments = Bundle().apply {
                        putSerializable(FIELD, field)
                    }
                }
        }
    }

    override fun setLayout() = R.layout.fragment_water_layer_editor

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        container.setOnClickListener { }
        clsProfile = getClsProfile() ?: return

        saveButton.setSafeOnClickListener {
            groundLayersActivity.hideKeyboard(view)
            groundLayersActivity.viewModel.setClsProfile(clsProfile)
            groundLayersActivity.onBackPressed()
        }

        field = arguments?.getSerializable(FIELD) as Field
        setEditorUi()
        setInputChangedListener()
    }

    private fun setEditorUi() {
        when (field) {
            is Field.EditWater -> {
                val profile = getClsProfile()
                if (profile != null) {
                    firstValueEditText.apply {
                        inputType =
                            (InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL)

                        val value =
                            profile.wody?.var_wod?.get((field as Field.EditWater).position)?.PWoda1?.toString()

                        if (value.isNullOrEmpty() || value == "0.0") {
                            setText("")
                            hint = "0.0"
                        } else {
                            setText(value)
                        }
                    }

                    secondValueEditText.apply {
                        inputType =
                            (InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL)

                        val value =
                            profile.wody?.var_wod?.get((field as Field.EditWater).position)?.PWoda2?.toString()

                        if (value.isNullOrEmpty() || value == "0.0") {
                            setText("")
                            hint = "0.0"
                        } else {
                            setText(value)
                        }
                    }
                }
            }

            is Field.EditFiltration -> {
                val profile = getClsProfile()
                if (profile != null) {
                    firstValueEditText.apply {
                        inputType =
                            (InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL)

                      val value =
                            profile.saczenia?.var_sacz?.get((field as Field.EditFiltration).position)?.PSacz1?.toString()

                        if (value.isNullOrEmpty() || value == "0.0") {
                            setText("")
                            hint = "0.0"
                        } else {
                            setText(value)
                        }
                    }

                    secondValueEditText.apply {
                        inputType =
                            (InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL)

                       val value = profile.saczenia?.var_sacz?.get((field as Field.EditFiltration).position)?.PSacz2?.toString()

                        if (value.isNullOrEmpty() || value == "0.0") {
                            setText("")
                            hint = "0.0"
                        } else {
                            setText(value)
                        }
                    }
                }
            }
            is Field.EditSamples -> {
                val profile = getClsProfile()
                if (profile != null) {
                    firstValueEditText.apply {
                        inputType =
                            InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL


                       val value = profile.Samples?.cls_Sample?.get((field as Field.EditSamples).position)?.depth?.mm?.millimetreToMeter()?.toString()

                        if (value.isNullOrEmpty() || value == "0.0") {
                            setText("")
                            hint = "0.0"
                        } else {
                            setText(value)
                        }
                    }
                    secondValueEditText.hide()
                }
            }
        }
    }

    private fun setInputChangedListener() {

        firstValueEditText.afterTextChanged { value ->
            Timber.d("value %s", value)
            when (field) {
                is Field.EditWater -> {
                    val profile = getClsProfile()
                    if (profile != null) {
                        profile.wody?.var_wod?.get((field as Field.EditWater).position)?.PWoda1 =
                            getDouble(value)
                    }
                }
                is Field.EditFiltration -> {
                    val profile = getClsProfile()
                    if (profile != null) {
                        profile.saczenia?.var_sacz?.get((field as Field.EditFiltration).position)?.PSacz1 =
                            getDouble(value)
                    }
                }
                is Field.EditSamples -> {
                    val profile = getClsProfile()
                    if (profile != null) {
                        profile.Samples?.cls_Sample?.get((field as Field.EditSamples).position)?.depth?.mm =
                            getDouble(value).meterToMillimetre()
                    }
                }
            }
        }

        secondValueEditText.afterTextChanged { value ->
            Timber.d("value %s", value)
            when (field) {
                is Field.EditWater -> {
                    val profile = getClsProfile()
                    if (profile != null) {
                        profile.wody?.var_wod?.get((field as Field.EditWater).position)?.PWoda2 =
                            getDouble(value)
                    }
                }
                is Field.EditFiltration -> {
                    val profile = getClsProfile()
                    if (profile != null) {
                        profile.saczenia?.var_sacz?.get((field as Field.EditFiltration).position)?.PSacz2 =
                            getDouble(value)
                    }
                }
            }
        }
    }

    private fun getClsProfile() = groundLayersActivity
        .viewModel
        .getClsProfile()
        .value

    private fun getDouble(value: String): Double {
        try {
            return value.toDouble()
        } catch (e: NumberFormatException) {
            Timber.e(e)
        }
        return 0.0
    }
}
