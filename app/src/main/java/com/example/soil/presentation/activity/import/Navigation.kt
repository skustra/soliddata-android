package com.example.soil.presentation.activity.import

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.soil.model.cls.SoilModel

sealed class Navigation {
    object SplashScreen : Navigation()
    object FilesScreen : Navigation()
    object ImportFileScreen : Navigation()
    data class FileNavigationScreen(val data: SoilModel) : Navigation()
}

class ImportFileNavigationViewModel : ViewModel() {

    private val navigation: MutableLiveData<Navigation> = MutableLiveData(
        Navigation.SplashScreen
    )

    fun getNavigation(): LiveData<Navigation> = navigation

    fun changeNavigation(navigation: Navigation) {
        this.navigation.value = navigation
    }
}