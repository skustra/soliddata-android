package com.example.soil.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.example.soil.presentation.activity.import.ImportFileActivity

abstract class BaseFragment : Fragment() {

    abstract fun setLayout(): Int

    private lateinit var navigationActivity: ImportFileActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater
            .from(inflater.context)
            .inflate(setLayout(), container, false)
    }

    fun <T : ViewModel> getViewModel(fragment: Fragment, viewModelClass: Class<T>): T {
        return activity?.run {
            ViewModelProviders.of(fragment)[viewModelClass]
        } ?: throw Exception("Invalid Activity")
    }

    fun <T : ViewModel> getViewModel(
        appCompatActivity: AppCompatActivity,
        viewModelClass: Class<T>
    ): T {
        return ViewModelProviders
            .of(appCompatActivity)
            .get(viewModelClass)
    }
}