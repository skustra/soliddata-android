package com.example.soil.presentation.fragment.files

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.net.toUri
import com.example.soil.R
import com.example.soil.model.cls.SoilModel
import com.example.soil.presentation.activity.import.ImportFileActivity
import com.example.soil.presentation.activity.import.Navigation
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.presentation.fragment.files.adapter.FilesAdapter
import com.example.soil.usecase.ExportSoilModelUseCase
import com.example.soil.usecase.ImportSoilModelUseCase
import com.example.soil.utils.RequestCodes
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.fragment_files.*
import utils.show
import java.io.File


class FilesFragment : BaseFragment() {

    private lateinit var importFileActivity: ImportFileActivity

    override fun setLayout(): Int {
        return R.layout.fragment_files
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        importFileActivity = context as ImportFileActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        checkPermissions()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCodes.GET_SOIL_FILE_CODE && resultCode == Activity.RESULT_OK) {
            val uri = data?.data ?: return
            openSoilModel(uri)

        }
    }

    private fun checkPermissions() = Dexter.withContext(importFileActivity)
        .withPermissions(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        .withListener(object : MultiplePermissionsListener {
            override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                if (report.areAllPermissionsGranted()) {
                    getSoilsFiles()
                    pickFile.setOnClickListener {
                        startActivityForResult(Intent(Intent.ACTION_GET_CONTENT).apply {
                            type = "*/*"
                        }, RequestCodes.GET_SOIL_FILE_CODE)
                    }
                }
            }

            override fun onPermissionRationaleShouldBeShown(
                p0: MutableList<PermissionRequest>?,
                p1: PermissionToken?
            ) {

            }
        }).check()

    private fun setSoilModel(soilModel: SoilModel?) {
        if (soilModel != null) {
            importFileActivity.viewModel.changeNavigation(
                Navigation.FileNavigationScreen(
                    soilModel
                )
            )
        }
    }

    fun getSoilsFiles() {
        val folder = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS
            ), "SoilData"
        )

        if (folder.exists() && folder.isDirectory) {
            val files: Array<File>? = folder.listFiles()?.apply {
                sortBy {
                    it.lastModified()
                }
                reverse()
            }

            if (files.isNullOrEmpty()) {
                noFilesView.show()
            } else {
                filesRecyclerView.adapter = FilesAdapter(files ?: arrayOf(), ::pickFile)
            }
        } else {
            noFilesView.show()
        }
    }

    private fun pickFile(file: File) {
        openSoilModel(file.toUri())
    }

    private fun openSoilModel(uri: Uri) {
        AlertDialog.Builder(importFileActivity)
            .create()
            .apply {
                setTitle(getString(R.string.question))
                setMessage(("${getString(R.string.impot_file_question)}\n\n${uri.path}"))
                setButton(Dialog.BUTTON_POSITIVE, getString(R.string.yes)) { dialog, _ ->
                    dialog.dismiss()
                    val soilModel = ImportSoilModelUseCase().invoke(uri, importFileActivity)
                    setSoilModel(soilModel)
                }
                setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.no)) { dialog, _ ->
                    dialog.dismiss()
                }
                show()
            }
    }
}