package com.example.soil.presentation.fragment.splash

import android.content.Context
import android.os.Bundle
import android.view.View
import com.example.soil.R
import com.example.soil.presentation.activity.import.ImportFileActivity
import com.example.soil.presentation.activity.import.Navigation
import com.example.soil.presentation.fragment.BaseFragment
import com.example.soil.utils.Run
import kotlinx.coroutines.GlobalScope

class SplashFragment : BaseFragment() {

    private lateinit var importFileActivity: ImportFileActivity

    override fun setLayout() = R.layout.fragment_splash

    override fun onAttach(context: Context) {
        super.onAttach(context)
        importFileActivity = context as ImportFileActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Run.after(mills = 1500, scope = GlobalScope) {
            importFileActivity.viewModel.changeNavigation(Navigation.ImportFileScreen)
        }
    }
}