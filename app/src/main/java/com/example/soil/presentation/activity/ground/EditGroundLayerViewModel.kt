package com.example.soil.presentation.activity.ground

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.soil.data.remote.ServiceBuilder
import com.example.soil.model.cls.ClsProf
import com.example.soil.model.cls.SoilModel
import com.example.soil.model.cls.VarGrunt
import com.example.soil.model.ground.SoilsResponse
import com.example.soil.model.humidities.HumidityResponse
import com.example.soil.model.rollings.RollingsResponse
import com.example.soil.model.rollings.RollingsResponseItem
import com.example.soil.model.states.Ground
import com.example.soil.usecase.MockSoilModel
import com.example.soil.usecase.getEmptyGround
import com.example.soil.utils.Constants.RETRY_DELAY
import com.example.soil.utils.Run
import com.example.soil.utils.toJson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.io.Serializable

sealed class Navigation {
    object GroundLayersList : Navigation()
    data class GroundLayerDetails(
        val groundIndex: Int
    ) : Navigation()

    object Loading : Navigation()
    data class EditGround(val field: Field, val groundIndex: Int) : Navigation()
    data class EditClsProfile(val field: Field) : Navigation()
    data class WaterLayers(val clsProf: ClsProf) : Navigation()
}

sealed class Field : Serializable {

    object Comment : Field()
    object Thickness : Field()
    data class ThicknessSum(
        var thicknessOneBeforeLast: Double,
        var thickness: Double,
        var thicknessSum: Double
    ) : Field()

    object Humidity : Field()
    object State : Field()
    object RollingsNumber : Field()
    object Colour : Field()
    object G1 : Field()
    object G2 : Field()
    object G3 : Field()
    object GroundName : Field()
    object Latitude : Field()
    object Longitude : Field()
    object Date : Field()
    object OrdinateOfMesurement : Field()

}

class EditGroundLayerViewModel : ViewModel() {

    private lateinit var humidity: HumidityResponse
    private lateinit var g1: SoilsResponse
    private lateinit var g2: SoilsResponse
    private lateinit var g3: SoilsResponse

    private var rollings: MutableLiveData<List<RollingsResponseItem>> = MutableLiveData(emptyList())
    private var states: MutableLiveData<ArrayList<Ground>> = MutableLiveData(arrayListOf())
    private lateinit var soilModel: SoilModel

    private val navigation: MutableLiveData<Navigation> =
        MutableLiveData(Navigation.Loading)

    fun getNavigation(): LiveData<Navigation> = navigation

    fun changeNavigation(navigation: Navigation) {
        this.navigation.value = navigation
    }

    private val clsProfileLiveData: MutableLiveData<ClsProf> = MutableLiveData()
    fun getClsProfile(): LiveData<ClsProf> = clsProfileLiveData

    fun setClsProfile(clsProf: ClsProf) {
        this.clsProfileLiveData.value = clsProf
    }

    fun setData(soilModel: SoilModel, profileNumber: String) {

        soilModel.cls_Project.Profil?.cls_Prof?.forEach {
            if (it.Nr == profileNumber) {
                this.clsProfileLiveData.value = it
            }
        }

        this.soilModel = soilModel
    }

    fun fetchSoilData() {
        fetchHumidity()
    }

    fun getHumidity() = humidity

    /* fun getDeliminations() = layers*/

    fun getRollings() = rollings.value ?: emptyList()
    fun getG1() = g1
    fun getG2() = g2
    fun getG3() = g3

    fun getRollingsLiveData(): LiveData<List<RollingsResponseItem>> = rollings
    fun getStatesLiveData(): LiveData<ArrayList<Ground>> = states

    fun getStates(): ArrayList<Ground> {
        return states.value ?: arrayListOf()
    }

    private fun fetchHumidity() {
        ServiceBuilder.request.getHumidity(isEuro = soilModel.cls_Project.Euro)
            .enqueue(object : Callback<HumidityResponse> {
                override fun onResponse(
                    call: Call<HumidityResponse>,
                    response: Response<HumidityResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        humidity = response.body()!!
                        fetchG1()
                    } else {
                        Timber.e("Error: %s", response.errorBody()?:"error")
                    }
                }

                override fun onFailure(call: Call<HumidityResponse>, t: Throwable) {
                    Timber.e("fetchHumidity error: %s", t.message)
                    Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                        fetchHumidity()
                    }
                }
            })
    }

    fun fetchG1() {
        ServiceBuilder.request.getSoils(isEuro = soilModel.cls_Project.Euro, g = "G1")
            .enqueue(object : Callback<SoilsResponse> {
                override fun onResponse(
                    call: Call<SoilsResponse>,
                    response: Response<SoilsResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        g1 = response.body()!!
                        fetchG2()
                    } else {
                        Timber.e("Error: %s", response.errorBody()?:"error")
                        Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                            fetchG1()
                        }
                    }
                }

                override fun onFailure(call: Call<SoilsResponse>, t: Throwable) {
                    Timber.e("fetchStates error: % s", t.message)
                    Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                        fetchG1()
                    }
                }
            })
    }

    fun fetchG2() {
        ServiceBuilder.request.getSoils(isEuro = soilModel.cls_Project?.Euro ?: false, g = "G2")
            .enqueue(object : Callback<SoilsResponse> {
                override fun onResponse(
                    call: Call<SoilsResponse>,
                    response: Response<SoilsResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        g2 = response.body()!!
                        fetchG3()
                    } else {
                        Timber.e("Error: %s", response.errorBody()?:"error")
                        Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                            fetchG2()
                        }
                    }
                }

                override fun onFailure(call: Call<SoilsResponse>, t: Throwable) {
                    Timber.e("fetchStates error: % s", t.message)
                    Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                        fetchG2()
                    }
                }
            })
    }

    fun fetchG3() {
        ServiceBuilder.request.getSoils(isEuro = soilModel.cls_Project?.Euro, g = "G3")
            .enqueue(object : Callback<SoilsResponse> {
                override fun onResponse(
                    call: Call<SoilsResponse>,
                    response: Response<SoilsResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        g3 = response.body()!!
                        navigation.value = Navigation.GroundLayersList
                    } else {
                        Timber.e("Error: %s", response.errorBody()?:"error")
                        Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                            fetchG3()
                        }
                    }
                }

                override fun onFailure(call: Call<SoilsResponse>, t: Throwable) {
                    Timber.e("fetchStates error: % s", t.message)
                    Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                        fetchG3()
                    }
                }
            })
    }

    fun getGround(groundIndex: Int): VarGrunt? {
        val clsProfile = clsProfileLiveData.value
        if (clsProfile != null) {
            return clsProfile.warstwy.var_grunt[groundIndex]
        }
        Timber.e("Return null ground.")
        return null
    }

    fun updateEditedGround(groundIndex: Int, ground: VarGrunt) {
        Timber.w("Update ground.")

        val clsProfile = clsProfileLiveData.value
        if (clsProfile != null) {
            clsProfile.warstwy.var_grunt[groundIndex] = ground
            clsProfileLiveData.value = clsProfile
        }
    }

    fun isEuroNorm(): Boolean {
        return soilModel.cls_Project?.Euro
    }

    fun getUpdatedSoilModel(): SoilModel? {
        Timber.d("getUpdatedSoilModel")
        if (::soilModel.isInitialized) {

            val profile = getClsProfile().value ?: return soilModel
            val clsProfileList = soilModel.cls_Project?.Profil?.cls_Prof ?: return soilModel

            for (i in clsProfileList.indices) {
                Timber.d(
                    "Cls profile was updated %s %s - %s", i,
                    clsProfileList[i].Nr,
                    profile.Nr
                )

                if (clsProfileList[i].Nr == profile.Nr) {
                    clsProfileList[i] = profile
                    break
                }
            }
            return soilModel
        } else return null
    }

    fun fetchStates(ground: String) {
        val groundId = g1.find { it.frak == ground }?.number ?: -1

        ServiceBuilder.request.getStates(isEuro = soilModel.cls_Project.Euro, g1 = groundId)
            .enqueue(object : Callback<ArrayList<Ground>> {
                override fun onResponse(
                    call: Call<ArrayList<Ground>>,
                    response: Response<ArrayList<Ground>>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        states.value = response.body() ?: arrayListOf()
                    } else {
                        Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                            fetchStates(ground)
                        }
                    }
                }

                override fun onFailure(call: Call<ArrayList<Ground>>, t: Throwable) {
                    Timber.e("fetchStates error: %s", t.message)
                    Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                        fetchStates(ground)
                    }
                }
            })
    }

    fun fetchRollings(ground: String) {
        ServiceBuilder.request.getRollings(
            isEuro = soilModel.cls_Project?.Euro,
            fraction = ground
        )
            .enqueue(object : Callback<RollingsResponse> {
                override fun onResponse(
                    call: Call<RollingsResponse>,
                    response: Response<RollingsResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        rollings.value = response.body()
                    } else {
                        Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                            fetchRollings(ground)
                        }
                        Timber.e("Error: %s", response.errorBody()?:"error")
                    }
                }

                override fun onFailure(call: Call<RollingsResponse>, t: Throwable) {
                    Timber.e("fetchRollings error: %s", t.message)
                    Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                        fetchRollings(ground)
                    }
                }
            })
    }

    fun fetchGroundName(varGround: VarGrunt, groundIndex: Int) {
        Timber.w("Fetch ground name")
        ServiceBuilder.request.getSoilName(
            isEuro = soilModel.cls_Project.Euro,
            g1 = g1.find { it.frak == varGround.G1 }?.number ?: -1,
            g2 = g2.find { it.frak == varGround.G2 }?.number ?: -1,
            g3 = g3.find { it.frak == varGround.G3 }?.number ?: -1,
        )
            .enqueue(object : Callback<String> {
                override fun onResponse(
                    call: Call<String>,
                    response: Response<String>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        varGround.POpisWarstwy = response.body()
                        updateEditedGround(groundIndex, varGround)
                    } else {
                        Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                            fetchGroundName(varGround, groundIndex)
                        }
                    }
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    Timber.w("fetchRollings error: %s", t.message)
                    Run.after(mills = RETRY_DELAY, scope = viewModelScope) {
                        fetchGroundName(varGround, groundIndex)
                    }
                }
            })
    }

    fun addGroundLayer() {
        val clsProfile = getClsProfile().value ?: return

        clsProfile.warstwy.var_grunt.add(
            MockSoilModel().getVarGround(
                isEuro = soilModel.cls_Project.Euro,
                number = clsProfile.warstwy.var_grunt.size + 1
            )
        )
        clsProfileLiveData.value = clsProfile
    }

    fun setDefaultRollings(groundIndex: Int) {
        val clsProfile = getClsProfile().value ?: return
        clsProfile.warstwy.var_grunt[groundIndex].NOR_e = "nn"
        clsProfile.warstwy.var_grunt[groundIndex].PNOR.LWal = "nn"
        clsProfileLiveData.value = clsProfile
    }

    fun setDefaultState(groundIndex: Int) {
        val clsProfile = getClsProfile().value ?: return
        clsProfile.warstwy.var_grunt[groundIndex].xml_Stan = "nn"
        clsProfileLiveData.value = clsProfile
    }

    fun removeGroundLayer(groundIndex: Int) {
        val clsProfile = getClsProfile().value ?: return
        clsProfile.warstwy.var_grunt.removeAt(groundIndex)

        for (i in clsProfile.warstwy.var_grunt.indices) {
            clsProfile.warstwy.var_grunt[i].NrWarGeo = (i + 1).toString()
        }

        clsProfileLiveData.value = clsProfile
    }

    fun updateSoilModel(soilModel: SoilModel) {
        Timber.d("updateSoilModel %s", soilModel.toJson())
        this.soilModel = soilModel

        this.soilModel.cls_Project.Profil?.cls_Prof?.forEach { profile ->
            Timber.d("%s %s", profile.Nr, getClsProfile().value?.Nr)
            if (profile.Nr == getClsProfile().value?.Nr) {
                Timber.e("Update")
                clsProfileLiveData.value = profile
            }
        }
        Timber.d("----")
        Timber.d("updatedSoilModel %s", soilModel.toJson())
    }

    fun findG1Ground(ground: String): String {
      return  getG1().find { it.frak == ground }?.nazwaGlowna?: getEmptyGround(soilModel.cls_Project.Euro)
    }

    fun findG2Ground(ground: String): String {
        return  getG2().find { it.frak == ground }?.nazwaGlowna?: getEmptyGround(soilModel.cls_Project.Euro)
    }

    fun findG3Ground(ground: String): String {
        return  getG3().find { it.frak == ground }?.nazwaGlowna?: getEmptyGround(soilModel.cls_Project.Euro)
    }
}