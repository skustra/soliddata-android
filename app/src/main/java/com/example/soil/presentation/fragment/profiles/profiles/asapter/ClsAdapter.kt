package com.example.soil.presentation.fragment.profiles.profiles.asapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.soil.R
import com.example.soil.model.cls.ClsProf
import com.example.soil.utils.millimetreToMeter
import kotlin.reflect.KFunction1

class ClsAdapter(
    val data: List<ClsProf>,
    val openClsProfile: KFunction1<String, Unit>,
    val removeProfile: KFunction1<Int, Unit>
) :
    RecyclerView.Adapter<ClsAdapter.LayersViewHolder>() {

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LayersViewHolder {
        return LayersViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_cls, parent, false)
        )
    }

    override fun onBindViewHolder(holder: LayersViewHolder, position: Int) {

        val item = data[position]

        holder.clsPosition.text = item.Nr
        holder.latitude.text = item.Y?.toString() ?: "0.0"
        holder.longitude.text = item.X?.toString() ?: "0.0"
        holder.ordinateOfTheHole.text = "${item.RzednaOtworu.mm.millimetreToMeter()} m ppt"

        holder.container.setOnClickListener {
            openClsProfile(item.Nr)
        }

        holder.container.setOnLongClickListener {
            removeProfile(position)
            true
        }
    }

    class LayersViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val ordinateOfTheHole: TextView = view.findViewById(R.id.ordinateOfTheHole)
        val latitude: TextView = view.findViewById(R.id.latitude)
        val longitude: TextView = view.findViewById(R.id.longitude)
        val clsPosition: TextView = view.findViewById(R.id.clsPosition)
        val container: LinearLayout = view.findViewById(R.id.container)
    }
}