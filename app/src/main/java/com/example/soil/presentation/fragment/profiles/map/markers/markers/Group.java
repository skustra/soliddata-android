package com.example.soil.presentation.fragment.profiles.map.markers.markers;

import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;

public class Group implements ClusterItem {

    public Group(double latitude, double longitude, String number) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.number = number;
    }

    private final String number;

    @SerializedName("latitude")
    @Expose
    private double latitude;

    @SerializedName("longitude")
    @Expose
    private double longitude;

    @Override
    public LatLng getPosition() {
        return new LatLng(latitude, longitude);
    }

    @Nullable
    @Override
    public String getTitle() {
        return number;
    }

    @Nullable
    @Override
    public String getSnippet() {
        return null;
    }
} 