package com.example.soil.data.remote

import com.example.soil.model.ground.SoilsResponse
import com.example.soil.model.humidities.HumidityResponse
import com.example.soil.model.rollings.RollingsResponse
import com.example.soil.model.states.Ground
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface SoilApi {

    @GET("/api/Soil/humidities")
    fun getHumidity(@Query("isEuro") isEuro: Boolean): Call<HumidityResponse>

    @GET("/api/Soil/states")
    fun getStates(@Query("isEuro") isEuro: Boolean, @Query("g1") g1: Int): Call<ArrayList<Ground>>

/*
    @GET("/api/Soil/layers")
    fun getLayers(
        @Query("isEuro") isEuro: Boolean
    ): Call<LayersResponse>
*/

    @GET("/api/Soil/rollings")
    fun getRollings(
        @Query("fraction") fraction: String,
        @Query("isEuro") isEuro: Boolean
    ): Call<RollingsResponse>

    @GET("/api/Soil/soils")
    fun getSoils(
        @Query("isEuro") isEuro: Boolean,
        @Query("g") g: String
    ): Call<SoilsResponse>

    @GET("/api/Soil/soilName")
    fun getSoilName(
        @Query("isEuro") isEuro: Boolean,
        @Query("g1") g1: Int,
        @Query("g2") g2: Int,
        @Query("g3") g3: Int
    ): Call<String>
}