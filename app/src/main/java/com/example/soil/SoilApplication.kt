package com.example.soil

import android.app.Application
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import timber.log.Timber

open class SoilApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        FirebaseApp.initializeApp(this)

        FirebaseAnalytics.getInstance(this)
            .setAnalyticsCollectionEnabled(true)
    }
}