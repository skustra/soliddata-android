package com.example.soil.usecase

import android.content.Context
import android.os.Environment
import android.widget.Toast
import com.example.soil.model.cls.SoilModel
import com.example.soil.utils.toJson
import fr.arnaudguyon.xmltojsonlib.JsonToXml
import timber.log.Timber
import java.io.*


class ExportSoilModelUseCase {

    private val folderName = "SoilData"

    fun getFilePath(): String {
        return "${Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOCUMENTS
        )}/$folderName"
    }

    fun invoke(context: Context, soilModel: SoilModel, fileName: String) {

        soilModel.fillSoilModelWithDefaultValues()

        val folder = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS
            ), folderName
        )

        if (!folder.exists()) {
            folder.mkdirs()
        }

        Timber.d("%s", soilModel.toJson())

        val xml = JsonToXml.Builder(soilModel.toJson())
            .build()
            .toFormattedString()

        val bufReader = BufferedReader(StringReader(xml))
        val builder = StringBuilder()
        var line: String? = null
        while (bufReader.readLine().also({ line = it }) != null) {
            val nonNullLine = line ?: ""
            when {
                nonNullLine.endsWith("/>") -> {
                    val withSpace = nonNullLine
                        .replace("/>", "")
                        .replace("<", "")
                        .replace(" ", "")

                    val withoutSpace = withSpace.replace(" ", "")
                    val final = "<$withSpace></$withoutSpace>"
                    Timber.d("Changed %s to %s final: %s ", nonNullLine, withoutSpace, final)
                    builder.append(final)
                }
                else -> {
                    builder.append(nonNullLine)
                }
            }
            builder.append('\n')
        }

        Timber.d("-----------")
        Timber.d("%s", builder.toString())
        try {
            val file = File(getFilePath(), "$fileName.SoilData")

            val fileOutputStream = FileOutputStream(file, true)
            val outWriter = OutputStreamWriter(fileOutputStream)
            outWriter.append(builder.toString())
            outWriter.close()
            fileOutputStream.close()
            Toast.makeText(context, "Zapisano.", Toast.LENGTH_LONG).show()
        } catch (e: IOException) {
            Timber.e("ERROR: %s", e.message)
            Toast.makeText(context, "Błąd. ${e.message}.", Toast.LENGTH_LONG).show()
        }
    }
}