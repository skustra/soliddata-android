package com.example.soil.usecase

import android.content.Context
import android.net.Uri
import android.widget.Toast
import com.example.soil.model.cls.SoilModel
import com.example.soil.utils.Constants
import com.example.soil.utils.FileUtils
import com.google.gson.Gson
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import timber.log.Timber
import java.io.BufferedReader
import java.io.FileReader

class ImportSoilModelUseCase {

    fun invoke(uri: Uri, context: Context): SoilModel? {
        val lastPathSegment = uri.lastPathSegment
        if (lastPathSegment.isNullOrEmpty()) {
            return null
        }

        val fileExtension = lastPathSegment.substring(lastPathSegment.lastIndexOf(".") + 1)
        Timber.d("Picked file extension: %s", fileExtension)

        if (fileExtension != Constants.SOIL_DATA_EXTENSION && fileExtension != Constants.XML_EXTENSION) {
            Toast.makeText(context, "Wybrany plik nie jest obesługiwany.", Toast.LENGTH_LONG).show()
            return null
        }

        val stream = context.contentResolver.openInputStream(uri) ?: return null

        val file = FileUtils.createFile(
            source = stream,
            cacheDir = "${context.cacheDir}/${"data.txt"}"
        )

        val fileTextValue = StringBuilder()

        try {
            val br = BufferedReader(FileReader(file))
            var line: String?
            while (br.readLine().also { line = it } != null) {

                val nonNullLine = line ?: ""

                Timber.d("line: %s", nonNullLine)
                when {
                    nonNullLine.endsWith("/>") -> {
                        val withSpace = nonNullLine
                            .replace("/>", "")
                            .replace("<", "")
                            .replace(" ", "")

                        val withoutSpace = withSpace.replace(" ", "")
                        val final = "<$withSpace></$withoutSpace>"
                        Timber.d("Changed %s to %s final: %s ", nonNullLine, withoutSpace, final)
                        fileTextValue.append(final)
                    }
                    nonNullLine == "<cls_Project xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" -> {
                        fileTextValue.append("<cls_Project>")
                    }
                    else -> {
                        fileTextValue.append(nonNullLine)
                    }
                }

                if (line?.contains("<wody>") == true) {
                    Timber.e("Added mock wody elements. Will be removed after object created..")
                    fileTextValue.append("<var_wod>\n</var_wod>")
                    fileTextValue.append('\n')
                    fileTextValue.append("<var_wod>\n</var_wod>")
                }

                if (line?.contains("<Samples>") == true) {
                    Timber.e("Added mock cls_Sample elements. Will be removed after object created..")
                    fileTextValue.append("<cls_Sample>\n</cls_Sample>")
                    fileTextValue.append('\n')
                    fileTextValue.append("<cls_Sample>\n</cls_Sample>")
                }
                if (line?.contains("<warstwy>") == true) {
                    Timber.e("Added mock var_grunt elements. Will be removed after object created..")
                    fileTextValue.append("<var_grunt>\n</var_grunt>")
                    fileTextValue.append('\n')
                    fileTextValue.append("<var_grunt>\n</var_grunt>")
                }
                if (line?.contains("<Profil>") == true) {
                    Timber.e("Added mock cls_prof elements. Will be removed after object created.")
                    fileTextValue.append("<cls_Prof>\n</cls_Prof>")
                    fileTextValue.append('\n')
                    fileTextValue.append("<cls_Prof>\n</cls_Prof>")
                }

                if (line?.contains("<saczenia>") == true) {
                    Timber.e("Added mock saczenia elements. Will be removed after object created.")
                    fileTextValue.append("<var_sacz>\n</var_sacz>")
                    fileTextValue.append('\n')
                    fileTextValue.append("<var_sacz>\n</var_sacz>")
                }
                fileTextValue.append('\n')
            }
            br.close()
            Timber.e("File -> Xml: %s", fileTextValue.toString())

            var jsonObj = XmlToJson.Builder(fileTextValue.toString()).build().toJson().toString()
            jsonObj = recreateNullValues(
                arrayOf(
                    "BGkolory",
                    "ClsProf",
                    "ClsProfWPrzek",
                    "cls_Project",
                    "clsPrzek",
                    "cls_Sample",
                    "ClsWarstwaGeo",
                    "depth",
                    "kol",
                    "kolory",
                    "Linie",
                    "Model",
                    "Plkkolory",
                    "PMiazszosc",
                    "PNOR",
                    "Profil",
                    "PrzekNrProfilu",
                    "Przekroj",
                    "PWilgot",
                    "RzednaOtworu",
                    "saczenia",
                    "Samples",
                    "SoilModel",
                    "SondaGleb",
                    "SondaRodzaj",
                    "VarGrunt",
                    "VarKol",
                    "VarLinia",
                    "var_sacz",
                    "VarWod",
                    "warstwy",
                    "WarstwyGeo",
                    "wody"
                ), jsonObj
            )

            val model = (Gson().fromJson<SoilModel>(
                jsonObj,
                SoilModel::class.java
            ))

            if (!model.cls_Project.Profil?.cls_Prof.isNullOrEmpty()) {
                model.cls_Project.Profil?.cls_Prof?.removeFirst()
            }

            if (!model.cls_Project.Profil?.cls_Prof.isNullOrEmpty()) {
                model.cls_Project.Profil?.cls_Prof?.removeFirst()
            }
            model.cls_Project.Profil?.cls_Prof?.forEach {
                if (!it.warstwy.var_grunt.isNullOrEmpty()) {
                    it.warstwy.var_grunt.removeFirst()
                }

                if (!it.warstwy.var_grunt.isNullOrEmpty()) {
                    it.warstwy.var_grunt.removeFirst()
                }

                if (!it.wody?.var_wod.isNullOrEmpty()) {
                    it.wody?.var_wod?.removeFirst()
                }

                if (!it.wody?.var_wod.isNullOrEmpty()) {
                    it.wody?.var_wod?.removeFirst()
                }

                if (!it.Samples?.cls_Sample.isNullOrEmpty()) {
                    it.Samples?.cls_Sample?.removeFirst()
                }

                if (!it.Samples?.cls_Sample.isNullOrEmpty()) {
                    it.Samples?.cls_Sample?.removeFirst()
                }

                if (!it.saczenia?.var_sacz.isNullOrEmpty()) {
                    it.saczenia?.var_sacz?.removeFirst()
                }

                if (!it.saczenia?.var_sacz.isNullOrEmpty()) {
                    it.saczenia?.var_sacz?.removeFirst()
                }
            }

            model.fillSoilModelWithDefaultValues()
            return model

        } catch (e: Exception) {
            Toast.makeText(
                context,
                "Wprowadzony plik jest uszkodzony lub błędny.",
                Toast.LENGTH_LONG
            ).show()
            Timber.e("Loading data error: %s", e.message)
        }
        return null
    }

    private fun recreateNullValues(vararg: Array<String>, json: String): String {
        var mappedJson = json
        vararg.forEach { value ->
            mappedJson = mappedJson.replace("$value\":\"\"", "$value\":{}")
        }
        return mappedJson
    }
}