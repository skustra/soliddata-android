package com.example.soil.usecase

import com.example.soil.model.cls.*
import com.google.gson.Gson
 fun getEmptyGround(isEuro: Boolean): String {
    return if (isEuro) "_brakEC" else "brak"
}

fun SoilModel.fillSoilModelWithDefaultValues() {
    this.cls_Project.Profil?.cls_Prof?.forEach { pofile ->
        pofile.warstwy.var_grunt.forEach { ground ->

            if (ground.G2.isEmpty()) {
                ground.G2 =
                    getEmptyGround(this.cls_Project.Euro)
            }

            if (ground.G3.isEmpty()) {
                ground.G3 =
                    getEmptyGround(this.cls_Project.Euro)
            }

            if (ground.xml_Stan.isEmpty()) {
                ground.xml_Stan = "nn"
            }

            if (ground.PWilgot.wilg.isEmpty()) {
                ground.PWilgot.wilg = "nn"
            }

            if (ground.PNOR.LWal.isEmpty()) {
                ground.PNOR.LWal = "nn"
            }

            if (ground.NOR_e.isEmpty()) {
                ground.NOR_e = "nn"
            }
        }
    }
}

class MockSoilModel {

    fun getSoilModel(euroNorm: Boolean, name: String): SoilModel {
        val soilModel = Gson().fromJson(
            "{\n" +
                    "   \"cls_Project\":{\n" +
                    "      \"Profil\":{\n" +
                    "         \"cls_Prof\":[\n" +
                    "         ]\n" +
                    "      },\n" +
                    "      \"GPS\":\"true\",\n" +
                    "      \"xmlns:xsd\":\"http:\\/\\/www.w3.org\\/2001\\/XMLSchema\",\n" +
                    "      \"wersja\":\"1\",\n" +
                    "      \"kodziczekwczytanego\":\"\",\n" +
                    "      \"Proj_ID\":\"5bcff5ab-c7f9-4804-80c3-b910a970c30f\"\n" +
                    "   }\n" +
                    "}", SoilModel::class.java
        )
        soilModel.cls_Project.Euro = euroNorm
        soilModel.cls_Project.NazwaProjektu = name
        return soilModel

    }

    fun getEmptyProfile(number: Int, latitude: Double, longitude: Double): ClsProf {
        val profile = Gson().fromJson(
            "    {\n" +
                    "               \"warstwy\":{},\n" +
                    "               \"Nr\":\"1\",\n" +
                    "             \n" +
                    "               \"Metryka\":\"true\",\n" +
                    "               \"SondaGleb\":{\n" +
                    "                  \"mm\":\"0\"\n" +
                    "               },\n" +
                    "               \"nazwa\":\"\",\n" +
                    "               \"sondaRysuj\":\"true\",\n" +
                    "               \"skalaSondaindex\":\"1\",\n" +
                    "               \"Sondowanie\":\"\",\n" +
                    "  \"saczenia\":{\n" +
                    "                  \"var_sacz\":[\n" +
                    "                  ]\n" +
                    "               }," +
                    " \"wody\":{\n" +
                    "                  \"var_wod\":[\n" +
                    "                  ]\n" +
                    "               }," +
                    "Samples\":{\n" +
                    "                  \"cls_Sample\": [ ]\n" +
                    "               },\n" +
                    "               \"skalaindex\":\"1\",\n" +
                    "               \"X\":\"$longitude\",\n" +
                    "               \"Y\":\"$latitude\",\n" +
                    "               \"RzednaOtworu\":{\n" +
                    "                  \"mm\":\"0\"\n" +
                    "               }\n" +
                    "         \n" +
                    "            }", ClsProf::class.java
        )

        val date = com.example.soil.utils.DateUtils.getCurrentDateSourceFormat()
        profile.apply {
            Data = date
            SondaData = date
            Nr = number.toString()
        }
        return profile
    }

    fun getVarGround(isEuro: Boolean, number: Int): VarGrunt {

        val ground: VarGrunt = Gson().fromJson<VarGrunt>(
            " {\n" +
                    "                        \"PWilgot\":{\n" +
                    "                           \"wilg\":\"\"\n" +
                    "                        },\n" +
                    "                        \"POpisWarstwy\":\"\",\n" +
                    "                        \"PNOR\":{\n" +
                    "                           \"LWal\":\"nn\"\n" +
                    "                        },\n" +
                    "                        \"NOR_e\":\"nn\",\n" +
                    "                        \"PColour\":\"\",\n" +
                    "                        \"G1\":\"\",\n" +
                    "                        \"G2\":\"${getEmptyGround(
                        isEuro
                    )}\",\n" +
                    "                        \"G3\":\"${getEmptyGround(
                        isEuro
                    )}\",\n" +
                    "                        \"PMiazszosc\":{\n" +
                    "                           \"mm\":\"0\"\n" +
                    "                        },\n" +
                    "                        \"NrWarGeo\":\"\",\n" +
                    "                        \"PPNG2\":\"0\",\n" +
                    "                        \"xml_Stan\":\"nn\",\n" +
                    "                        \"wilg_e\":\"mw\"\n" +
                    "                     }", VarGrunt::class.java
        )
        ground.NrWarGeo = number.toString()
        return ground
    }

    fun getClsSample(): ClsSample {
        return Gson().fromJson(
            "{\n" +
                    "                     \"depth\":{\n" +
                    "                        \"mm\":\"0.0\"\n" +
                    "                     },\n" +
                    "                     \"rodzaj\":\"zw\"\n" +
                    "                  }", ClsSample::class.java
        )
    }

    fun getFiltration(): VarSacz {
        return Gson().fromJson(
            "{\n" +
                    "                     \"PSacz1\":\"0.0\",\n" +
                    "                     \"PSacz2\":\"0.0\"\n" +
                    "                  }", VarSacz::class.java
        )
    }

    fun getWater(): VarWod {
        return Gson().fromJson(
            "{\n" +
                    "                        \"PWoda2\":\"0.0\",\n" +
                    "                        \"PWoda1\":\"0.0\"\n" +
                    "                     }", VarWod::class.java
        )
    }
}