package com.example.soil.utils

import com.google.gson.Gson

fun Any.toJson(): String {
    return Gson().toJson(this)
}