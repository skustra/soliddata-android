package com.example.soil.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.soil.presentation.fragment.BaseFragment
import timber.log.Timber

fun AppCompatActivity.replaceFragment(fragment: BaseFragment, id: Int) {
    Timber.d("Navigation: replaceFragment: %s", fragment.javaClass)
    this.supportFragmentManager
        .beginTransaction()
        .replace(id, fragment)
        .commit()
}

fun Fragment.replaceChildFragment(fragment: Fragment, id: Int) {
    Timber.d("Navigation: replaceChildFragment: %s", fragment.javaClass)
    this.childFragmentManager
        .beginTransaction()
        .replace(id, fragment)
        .commit()
}

fun AppCompatActivity.addFragmentWithBackStack(fragment: BaseFragment, id: Int) {

    val fragmentInStack = supportFragmentManager.findFragmentByTag(fragment.javaClass.name)
    if (fragmentInStack != null) {
        Timber.d("Remove fragment from stack: %s", fragment)
        supportFragmentManager.beginTransaction()
            .remove(fragmentInStack)
            .commit()
    }

    Timber.d("Navigation: addFragmentWithBackStack: %s", fragment.javaClass)
    this.supportFragmentManager
        .beginTransaction()
        .add(id, fragment, fragment.javaClass.name)
        .addToBackStack(fragment.javaClass.name)
        .commit()
}

fun Fragment.addChildFragmentWithBackStack(fragment: BaseFragment, id: Int) {
    Timber.d("Navigation: addChildFragmentWithBackStack: %s", fragment.javaClass)
    this.childFragmentManager
        .beginTransaction()
        .add(id, fragment)
        .addToBackStack(fragment.javaClass.name)
        .commit()
}

fun AppCompatActivity.removeFragmentFromStack(fragment: BaseFragment) {
    Timber.d("Navigation: removeFragmentFromStack: %s", fragment.javaClass)
    this.supportFragmentManager
        .beginTransaction()
        .remove(fragment)
        .commit()
}

fun AppCompatActivity.removeFragmentFromStack(tag: String) {
    Timber.d("Navigation removeFragmentFromStack: %s", tag)
    this.supportFragmentManager.findFragmentByTag(tag)?.let { fragment ->
        Timber.w("Navigation Remove fragment: %s", fragment.javaClass.toString())
        this.supportFragmentManager.beginTransaction()
            .remove(fragment)
            .commitAllowingStateLoss()
    }
}

fun FragmentManager.clear(id: Int) {
    findFragmentById(id)?.let { fragment ->
        Timber.w("Navigation Remove fragment: %s", fragment.javaClass.toString())
        beginTransaction()
            .remove(fragment)
            .commitAllowingStateLoss()
    }
}
