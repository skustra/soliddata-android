package com.example.soil.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class Run {
    companion object {
        fun after(scope: CoroutineScope, mills: Long, process: () -> Unit) {
            scope.launch(Dispatchers.Main) {
                delay(mills)
                process()
            }
        }
    }
}