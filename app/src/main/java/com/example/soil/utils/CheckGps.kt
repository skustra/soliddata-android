package com.example.soil.utils

import android.content.Context
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResult

class CheckGps constructor(context: Context) {

    private val locationRequest = LocationRequest().apply {
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        interval = 5
        fastestInterval = 3
    }

    private var settings: LocationSettingsRequest = LocationSettingsRequest.Builder()
        .addLocationRequest(locationRequest)
        .setAlwaysShow(true)
        .build()

    private val googleApiClient: GoogleApiClient = GoogleApiClient.Builder(context)
        .addApi(LocationServices.API)
        .build()

    fun invoke(): PendingResult<LocationSettingsResult>? {
        if (!googleApiClient.isConnected) {
            googleApiClient.connect()
        }
        return LocationServices.SettingsApi
            .checkLocationSettings(googleApiClient, settings)
    }
}