package com.example.soil.utils

import timber.log.Timber
import java.io.*

class FileUtils {

    companion object {
        fun createFile(source: InputStream, cacheDir: String): File? {
            try {
                val f = File(cacheDir)
                f.createNewFile()
                val outputStream: OutputStream = FileOutputStream(f)
                val buffer = ByteArray(1024)
                var length = 0
                while (source.read(buffer).also { length = it } > 0) {
                    outputStream.write(buffer, 0, length)
                }
                outputStream.close()
                source.close()
                return f
            } catch (e: IOException) {
                Timber.e(e.message)
            }
            return null
        }
    }
}