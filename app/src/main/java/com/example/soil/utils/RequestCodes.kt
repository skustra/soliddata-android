package com.example.soil.utils

object RequestCodes {

    const val GET_SOIL_FILE_CODE = 1
    const val EDIT_FILE_CODE = 2

}