package com.example.soil.utils

import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class DateUtils {

    companion object {

        const val XML_SOURCE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"
        const val XML_SOURCE_DATE_FORMAT_SAVE = "yyyy-MM-dd'T'HH:mm:ssXXX"
        const val FRONT_END_DATE_FORMAT = "yyyy-MM-dd HH:mm"

        fun formatDate(dateValue: String): String {
            val sourceFormat = SimpleDateFormat(XML_SOURCE_DATE_FORMAT, Locale.getDefault())
            return try {
                val date = sourceFormat.parse(dateValue)
                val formatter = SimpleDateFormat(FRONT_END_DATE_FORMAT, Locale.getDefault())

                if (date == null)
                    return dateValue

                formatter.format(date)
            } catch (e: Exception) {
                Timber.e("formatDate: %s", e.message)
                dateValue
            }
        }

        fun getCurrentDateSourceFormat(): String {
            val calendar = Calendar.getInstance()
            val formatter = SimpleDateFormat(XML_SOURCE_DATE_FORMAT_SAVE, Locale.getDefault())
            return formatter.format(calendar.time)
        }
    }
}