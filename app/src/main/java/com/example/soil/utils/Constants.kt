package com.example.soil.utils

/*
  1.) Edytowalność daty pomiaru. ok
  2.) Edytowalność rzędnej pomiaru. ok
  5.) Przekazuje paramentr true false do API ok
  8.) Nazwa aplikacji -> SoilData ok
  9.) Logo wyciągnąć z PDF'a z email. ok

  PROGRESS  6.) Gdy norma to EUR to uzupełniamy 3 grunty. (Główna G1, składowa drugorzędna G2, składowa trzeciorzędna G3)

   3.) Liczba wałeczkowa - NOR
   4.) Grunty (G1,  G2,G3) - Rozdzielamy na Polską norme/europejską
   7.) Gdy norma to EUR -> Na detalach warstwy gruntu pokazujemy zamiast Gruntu G1,G2,G3 osobno. Wybieramy G1 -> wybieramy grunt. Wybieramy G2 -> Wybieramy grunt. ZAWSZE Z LISTY GRUNT


   10.) W polskiej normie G1 to składowa główna, natomiast g2 to przewarstwienie, a g3 to dodatki (G3 jest pobierane/ustawiane jak w euronormie).

   */

object Constants {
    const val RETRY_DELAY = 2000L
    const val EMPTY = ""
    const val PRETTY_PRINT_INDENT_FACTOR = 2
    const val SOIL_DATA_EXTENSION = "SoilData"
    const val XML_EXTENSION = "xml"
    const val SOIL_MODEL = "soil_data"
    const val GROUND_LAYER = "VarGrunt"
    const val PROFILE_NUMBER = "profile_id"
    const val GROUND_INDEX = "var_grunt_position"
    const val FIELD = "field"
    const val ONE_BEFORE_LAST_THICKNESS = "one_before_last_thickness"
}