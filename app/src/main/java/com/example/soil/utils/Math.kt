package com.example.soil.utils

fun Int.millimetreToMeter(): Double {
    return this / 1000.0
}

fun Double.millimetreToMeter(): Double {
    return this / 1000.0
}

fun Int.meterToMillimetre(): Double {
    return this * 1000.0
}

fun Double.meterToMillimetre(): Double {
    return this * 1000.0
}